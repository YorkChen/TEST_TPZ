/************************************************************************/
// FILE NAME: apt_type.h
// DATE:      2014. 03. 21
// COMPANY:		THORLABS
// Author:    eason dai
// DESCRIPTION: Defines apt types
/************************************************************************/
#ifndef APT_TYPE_H
#define APT_TYPE_H
/****************************************************************************************/
//
//	Request command messages "APT_COMMAND_MESSAGE_TYPE"
//
/****************************************************************************************/
typedef enum
{
    APT_MGMSG_MOD_IDENTIFY = 0x0223,
    APT_MGMSG_MOD_SET_CHANENABLESTATE = 0x0210,
    APT_MGMSG_MOD_REQ_CHANENABLESTATE = 0x0211,
    APT_MGMSG_HW_RESPONSE = 0x0080,
    APT_MGMSG_HW_START_UPDATEMSGS = 0x0011,
    APT_MGMSG_HW_STOP_UPDATEMSGS = 0x0012,
    APT_MGMSG_HW_REQ_INFO = 0x0005,
    APT_MGMSG_MOT_SET_POSCOUNTER = 0x0410,
    APT_MGMSG_MOT_REQ_POSCOUNTER = 0x0411,
    APT_MGMSG_MOT_SET_ENCCOUNTER = 0x0409,
    APT_MGMSG_MOT_REQ_ENCCOUNTER = 0x040A,
    APT_MGMSG_MOT_SET_VELPARAMS = 0x0413,
    APT_MGMSG_MOT_REQ_VELPARAMS = 0x0414,
    APT_MGMSG_MOT_SET_JOGPARAMS = 0x0416,
    APT_MGMSG_MOT_REQ_JOGPARAMS = 0x0417,
    APT_MGMSG_MOT_SET_GENMOVEPARAMS = 0x043A,
    APT_MGMSG_MOT_REQ_GENMOVEPARAMS = 0x043B,
    APT_MGMSG_MOT_SET_MOVERELPARAMS = 0x0445,
    APT_MGMSG_MOT_REQ_MOVERELPARAMS = 0x0446,
    APT_MGMSG_MOT_SET_MOVEABSPARAMS = 0x0450,
    APT_MGMSG_MOT_REQ_MOVEABSPARAMS = 0x0451,
    APT_MGMSG_MOT_SET_HOMEPARAMS = 0x0440,
    APT_MGMSG_MOT_REQ_HOMEPARAMS = 0x0441,
    APT_MGMSG_MOT_SET_LIMSWITCHPARAMS = 0x0423,
    APT_MGMSG_MOT_REQ_LIMSWITCHPARAMS = 0x0424,
    APT_MGMSG_MOT_MOVE_HOME = 0x0443,
    APT_MGMSG_MOT_MOVE_RELATIVE = 0x0448,
    APT_MGMSG_MOT_MOVE_ABSOLUTE = 0x0453,
    APT_MGMSG_MOT_MOVE_JOG = 0x046A,
    APT_MGMSG_MOT_MOVE_VELOCITY = 0x0457,
    APT_MGMSG_MOT_MOVE_STOP = 0x0465,
    APT_MGMSG_MOT_SET_DCPIDPARAMS = 0x04A0,
    APT_MGMSG_MOT_REQ_DCPIDPARAMS = 0x04A1,
    APT_MGMSG_MOT_SET_AVMODES = 0x04B3,
    APT_MGMSG_MOT_REQ_AVMODES = 0x04B4,
    APT_MGMSG_MOT_SET_POTPARAMS = 0x04B0,
    APT_MGMSG_MOT_REQ_POTPARAMS = 0x04B1,
    APT_MGMSG_MOT_SET_BUTTONPARAMS = 0x04B6,
    APT_MGMSG_MOT_REQ_BUTTONPARAMS = 0x04B7,
    APT_MGMSG_MOT_REQ_DCSTATUSUPDATE = 0x0490 ,
    APT_MGMSG_MOT_ACK_DCSTATUSUPDATE = 0x0492,
    APT_MGMSG_MOT_REQ_STATUSBITS = 0x0429,
    APT_MGMSG_MOT_SUSPEND_ENDOFMOVEMSGS = 0x046B,
    APT_MGMSG_MOT_RESUME_ENDOFMOVEMSGS = 0x046C,
    APT_MGMSG_MOT_SET_POWERPARAMS = 0x0426,
    APT_MGMSG_MOT_REQ_POWERPARAMS = 0x0427,
    APT_MGMSG_HW_DISCONNECT = 0x0002,
    APT_MGMSG_HUB_REQ_BAYUSED = 0x0065,

    //TPZ MSG
    APT_MGMSG_PZ_SET_OUTPUTVOLTS = 0x0643,
    APT_MGMSG_PZ_REQ_OUTPUTVOLTS = 0x0644,
    APT_MGMSG_PZ_SET_POSCONTROLMODE = 0x0640,
    APT_MGMSG_PZ_REQ_POSCONTROLMODE = 0x0641,
    APT_MGMSG_PZ_SET_OUTPUTPOS = 0x0646,
    APT_MGMSG_PZ_REQ_OUTPUTPOS = 0x0647,
    APT_MGMSG_PZ_SET_INPUTVOLTSSRC = 0x0652,
    APT_MGMSG_PZ_REQ_INPUTVOLTSSRC = 0x0653,
    APT_MGMSG_PZ_SET_PICONSTS = 0x0655,
    APT_MGMSG_PZ_REQ_PICONSTS = 0x0656,
    APT_MGMSG_PZ_SET_TPZ_DISPSETTINGS = 0x07D1,
    APT_MGMSG_PZ_REQ_TPZ_DISPSETTINGS = 0x07D2,
    APT_MGMSG_PZ_SET_IOSETTINGS = 0x07D4,
    APT_MGMSG_PZ_REQ_IOSETTINGS = 0x07D5
}APT_COMMAND_MESSAGE_TYPE;

/****************************************************************************************/
//
//	Response command messages "APT_RESPONSE_MESSAGE_TYPE"
//
/****************************************************************************************/
typedef enum
{
    APT_MGMSG_MOT_ERROR = -1,
    APT_MGMSG_MOT_NOT_ENOUGH_LENGTH = 0,
    APT_MGMSG_MOD_GET_CHANENABLESTATE = 0x0212,
    APT_MGMSG_HW_GET_INFO = 0x0006,
    APT_MGMSG_MOT_GET_POSCOUNTER = 0x0412,
    APT_MGMSG_MOT_GET_ENCCOUNTER = 0x040B,
    APT_MGMSG_MOT_GET_VELPARAMS = 0x0415,
    APT_MGMSG_MOT_GET_JOGPARAMS = 0x0418,
    APT_MGMSG_MOT_GET_GENMOVEPARAMS = 0x043C,
    APT_MGMSG_MOT_GET_MOVERELPARAMS = 0x0447,
    APT_MGMSG_MOT_GET_MOVEABSPARAMS = 0x0452,
    APT_MGMSG_MOT_GET_HOMEPARAMS = 0x0442,
    APT_MGMSG_MOT_GET_LIMSWITCHPARAMS = 0x0425,
    APT_MGMSG_MOT_MOVE_HOMED = 0x0444,
    APT_MGMSG_MOT_MOVE_COMPLETED = 0x0464,
    APT_MGMSG_MOT_MOVE_STOPPED = 0x0466,
    APT_MGMSG_MOT_GET_DCPIDPARAMS = 0x04A2,
    APT_MGMSG_MOT_GET_AVMODES = 0x04B5,
    APT_MGMSG_MOT_GET_POTPARAMS = 0x04B2,
    APT_MGMSG_MOT_GET_BUTTONPARAMS = 0x04B8,
    APT_MGMSG_MOT_GET_DCSTATUSUPDATE = 0x0491,
    APT_MGMSG_MOT_GET_STATUSBITS = 0x042A,
    APT_MGMSG_MOT_GET_POWERPARAMS = 0x0428,
    APT_MGMSG_HW_RICHRESPONSE = 0x0081,
    APT_MGMSG_HUB_GET_BAYUSED = 0x0060,

    //TPZ RESPONSE
    APT_MGMSG_PZ_GET_POSCONTROLMODE = 0x0642,
    APT_MGMSG_PZ_GET_OUTPUTVOLTS = 0x0645,
    APT_MGMSG_PZ_GET_OUTPUTPOS = 0x0648,
    APT_MGMSG_PZ_GET_INPUTVOLTSSRC = 0x0654,
    APT_MGMSG_PZ_GET_PICONSTS = 0x0657,
    APT_MGMSG_PZ_GET_TPZ_DISPSETTINGS = 0x07D3,
    APT_MGMSG_PZ_GET_IOSETTINGS = 0x07D6

}APT_RESPONSE_MESSAGE_TYPE;

/****************************************************************************************/
//
//	Size of response command messages "APT_RESPONSE_MESSAGE_SIZE"
//
/****************************************************************************************/
typedef enum
{
    APT_MGMSG_MOT_ERROR_SIZE = 0,
    APT_MGMSG_MOT_NOT_ENOUGH_LENGTH_SIZE = 0,
}APT_RESPONSE_MESSAGE_SIZE;

typedef struct
{
    APT_RESPONSE_MESSAGE_TYPE res_msg_type;
    int  res_msg_size;
}RESPONSE_MSG;
/****************************************************************************************/
//
//	Value type of commands' params
//
/****************************************************************************************/
typedef enum 
{
    MOV_FORWARD = 0x01,
    MOV_REVERSE = 0x02
}MOVE_DIRECTION;

typedef enum
{
    STOP_IMMEDIATE = 0x01,			///stop immediately
    PROFILED_STOP = 0x02        ///stop in a m_controller(profiled) manner.
}MOVE_STOP_MODE;

typedef enum
{
    SINGLE_STEP_JOGGING = 0x01,			///single step jogging
    CONTINUS_JOGGING = 0x02				///continus jogging
}JOG_MODE;

typedef enum
{
    IGNORE_SWITCH_OR_SWITCH_NOT_PRESENT = 0x01,
    SWITCH_MAKES_ON_CONTACT,
    SWITCH_BREAKS_ON_CONTACT,
    SWITCH_MAKES_ON_CONTACT_ONLY_FOR_HOMES,
    SWITCH_BREAKS_ON_CONTACT_ONLY_FOR_HOMES,
    FOR_PMD_BASED_BRUSHLESS_SERVO_CONTROLLERS,
    BITWISE_OR_WITH_ONE_OF_THE_SETTINGS_ABOVE = 0x08

}CW_HAEDLIMIT;

typedef enum
{
    IGORE_LIMIT = 0x01,
    STOP_IMMEDIATE_AT_LIMIT,
    PROFILED_STOP_AT_LIMIT,
    ROTATION_STOP_LIMIT = 0x08

}SOFTWARE_LIMIT_MODE;

typedef enum
{
    LEDMODE_IDENT = 0x01,
    LEDMODE_LIMITSWITCH = 0x02,
    LEDMODE_MOVING = 0x08
}LEDMODE;

typedef enum
{
    BTN_JOG_MODE = 0x01,
    BTN_SPECIAL_MODE = 0x02
}BUTTON_MODE;

typedef enum
{
    MULTI_CHANNEL = 45,
    BRUSHLESS_DC = 44
}HARDWARE_TYPE;

typedef enum {
    FORWARD_HARDWARE_LIMIT_SWITCH = 0x00000001,
    REVERSE_HARDWARE_LIMIT_SWITCH = 0x00000002,
    MOVING_FORWARD = 0x00000010,
    MOVING_REVERSE = 0x00000020,
    JOGGING_FORWARD = 0x00000040,
    JOGGING_REVERSE = 0x00000080,
    HOMING = 0x00000200,
    HOMED = 0x00000400,
    TRACKING = 0x00001000,
    SETTLED = 0x00002000,
    MOTION_ERROR = 0x00004000,
    MOTOR_CURRENT_LIMIT_REACHED = 0x01000000,
    CHANNEL_ENABLED = 0x80000000,
}STATUS_BITS_FLAGS;

typedef enum
{
    OPEN_LOOP = 0x01,
    CLOSED_LOOP = 0x02,
    OPEN_LOOP_SMOOTH = 0x03,
    CLOSED_LOOP_SMOOTH = 0x04
}CONTROL_MODE;

typedef enum
{
    INPUT_HUB_1 = 0x01,
    INPUT_HUB_2 = 0x02,
    INPUT_SMA = 0x03
}HUB_ANALOGINPUT;

typedef enum
{
    INPUTSRC_SOFTWARE = 0x00,
    INPUTSRC_EXT = 0x01,
    INPUTSRC_PTM = 0x02,
    INPUTSRC_PTM_EXT = 0x03
}INPUTSRC_MODE;


/****************************************************************************************/
//
//	Data struct of commands' params
//
/****************************************************************************************/
typedef struct
{
    unsigned char cid;
    unsigned char d;
    unsigned char s;
    unsigned char enablesatus;
}APT_MGMSG_MOD_GET_CHANENABLESTATE_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    long serial_number;
    char mode_number[8];
    unsigned short type;
    unsigned char firmware_version[4];
    char notes[48];
    unsigned char empty_space[12];
    unsigned short hw_version;
    unsigned short mode_state;
    unsigned short nchs;
}APT_MGMSG_HW_GET_INFO_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    double position;
}APT_MGMSG_MOT_GET_POSCOUNTER_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    double encoder_counter;
}APT_MGMSG_MOT_GET_ENCCOUNTER_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    double min_vel;
    double acc;
    double max_vel;
}APT_MGMSG_MOT_GET_VELPARAMS_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    unsigned short jog_mode;
    double jog_step_size;
    double jog_min_vel;
    double jog_acc;
    double jog_max_vel;
    unsigned short jog_stop_mode;
}APT_MGMSG_MOT_GET_JOGPARAMS_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    double backlash_distance;
}APT_MGMSG_MOT_GET_GENMOVEPARAMS_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    double relative_distance;
}APT_MGMSG_MOT_GET_MOVERELPARAMS_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    double absolute_distance;
}APT_MGMSG_MOT_GET_MOVEABSPARAMS_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    unsigned short home_dir;
    unsigned short limit_switch;
    double home_vel;
    double offset_dis;
}APT_MGMSG_MOT_GET_HOMEPARAMS_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    unsigned short cw_hardlimit;
    unsigned short ccw_hardlimit;
    double cw_softlimit;
    double ccw_softlimit;
    unsigned short limit_mode;
}APT_MGMSG_MOT_GET_LIMSWITCHPARAMS_T;

typedef struct
{
    unsigned char cid;
    unsigned char d;
    unsigned char s;
}APT_MGMSG_MOT_MOVE_HOMED_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    double positon;
    double velocity;
    unsigned long statusbits;
}APT_MGMSG_MOT_MOVE_COMPLETED_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    double positon;
    double velocity;
    unsigned long statusbits;
}APT_MGMSG_MOT_MOVE_STOPPED_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    unsigned long int proportional;
    unsigned long int integral;
    unsigned long int differential;
    unsigned long int integral_limit;
    unsigned short filtercontrol;
}APT_MGMSG_MOT_GET_DCPIDPARAMS_T;

typedef struct 
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    unsigned short modebites;
}APT_MGMSG_MOT_GET_AVMODES_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    unsigned short zero_wnd;
    double vel1;
    unsigned short wnd1;
    double vel2;
    unsigned short wnd2;
    double vel3;
    unsigned short wnd3;
    double vel4;
}APT_MGMSG_MOT_GET_POTPARAMS_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    unsigned short mode;
    double position1;
    double position2;
    unsigned short timeout;
    unsigned short notused;
}APT_MGMSG_MOT_GET_BUTTONPARAMS_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    double positon;
    double velocity;
    unsigned long statusbits;
}APT_MGMSG_MOT_GET_DCSTATUSUPDATE_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    unsigned long statusbits;
}APT_MGMSG_MOT_GET_STATUSBITS_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    unsigned short restfactor;
    unsigned short movefactor;
}APT_MGMSG_MOT_GET_POWERPARAMS_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    unsigned char update_rate;
}APT_MGMSG_HW_START_UPDATEMSGS_T;

typedef struct  
{
    unsigned char d;
    unsigned char s;
    unsigned short scid;
    double relative_distance;
}APT_MGMSG_MOT_MOVE_RELATIVE_T;

typedef struct
{
    unsigned short msg_ident;
    unsigned short code;
    char notes[64];
}APT_MGMSG_HW_RICHRESPONSE_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    unsigned char bay_ident;
}APT_MGMSG_HUB_GET_BAYUSED_T;

typedef struct
{
    unsigned char cid;
    unsigned char d;
    unsigned char s;
    double volts;
}APT_MGMSG_PZ_GET_OUTPUTVOLTS_T;

typedef struct
{
    unsigned char cid;
    unsigned char d;
    unsigned char s;
    double percent;
}APT_MGMSG_PZ_GET_OUTPUTPOS_T;


typedef struct
{
    unsigned char cid;
    unsigned char d;
    unsigned char s;
    unsigned char mode;
}APT_MGMSG_PZ_GET_CONTROLMODE_T;

typedef struct
{
    unsigned char cid;
    unsigned char d;
    unsigned char s;
    double maxVolt;
    unsigned short hubAnalog;
}APT_MGMSG_PZ_GET_IOSETTINGS_T;

typedef struct
{
    unsigned char cid;
    unsigned char d;
    unsigned char s;
    unsigned char mode;
}APT_MGMSG_PZ_GET_INPUTVOLTSOURCE_T;

typedef struct
{
    unsigned char cid;
    unsigned char d;
    unsigned char s;
    int prop;
    int integral;
}APT_MGMSG_PZ_GET_PICONSTS_T;

typedef struct
{
    unsigned char d;
    unsigned char s;
    int intensity;
}APT_MGMSG_PZ_GET_DISPSETTING_T;
#endif //APT_TYPE_H
