/************************************************************************/
// FILE NAME :		GenericInstrument.cpp
// VERSION :
// DATE :			2013. 01. 22
// COMPANY :		thorlabs
// DESCRIPTION : Defines functions of constructing apt messages
//		v 0.1 : 2013. 01. 22 start project
// Author :		eason dai(edai@thorlabs.com)
//
/************************************************************************/
#ifndef GENERIC_INSTRUMENT_H
#define GENERIC_INSTRUMENT_H

#include "WinDef.h"
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// MGMSG
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int MGMSG_MOD_SET_CHANENABLESTATE(UCHAR *b, UCHAR cid, UCHAR en, UCHAR d, UCHAR s);
int MGMSG_MOD_REQ_CHANENABLESTATE(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s);
int MGMSG_MOD_IDENTIFY(UCHAR *b, UCHAR d, UCHAR s);
int MGMSG_HW_RESPONSE(UCHAR *b, UCHAR d, UCHAR s);
int MGMSG_HW_START_UPDATEMSGS(UCHAR *b,UCHAR updaterate,UCHAR unused,UCHAR d,UCHAR s);
int MGMSG_HW_STOP_UPDATEMSGS(UCHAR *b, UCHAR d, UCHAR s);
int MGMSG_HW_REQ_INFO(UCHAR *b,UCHAR d,UCHAR s);
int MGMSG_HW_DISCONNECT(UCHAR *b, UCHAR d, UCHAR s);
int MGMSG_HUB_REQ_BAYUSED(UCHAR *b, UCHAR d, UCHAR s);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void MGMSG_HW_GET_INFO(CHAR *Msg_Data, UCHAR &d, UCHAR &s, LONG &serial_number, CHAR *mode_number,
                       WORD &type, BYTE *firmware_version, CHAR *notes, BYTE* empty_space, WORD &hw_version, WORD &mode_state, WORD &nchs);
void MGMSG_HW_RICHRESPONSE(CHAR *Msg_Data, UCHAR &d, UCHAR &s, WORD &msgident,WORD &code, CHAR *notes);
void MGMSG_HUB_GET_BAYUSED(CHAR *Msg_Data, UCHAR &bay_ident, UCHAR &d, UCHAR &s);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// MGMSG_PZ
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int MGMSG_PZ_SET_POSCONTROLMODE(UCHAR *b, UCHAR cid, UCHAR mode, UCHAR d, UCHAR s);
int MGMSG_PZ_REQ_POSCONTROLMODE(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s);
int MGMSG_PZ_GET_POSCONTROLMODE(UCHAR *b, UCHAR &cid, UCHAR &mode, UCHAR &d, UCHAR &s);

int MGMSG_PZ_SET_OUTPUTVOLTS(UCHAR *b, UCHAR cid, double volt, UCHAR d, UCHAR s,double maxV);
int MGMSG_PZ_REQ_OUTPUTVOLTS(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s);
int MGMSG_PZ_GET_OUTPUTVOLTS(UCHAR *b, UCHAR &cid, double &volt, UCHAR &d, UCHAR &s,double maxV);

int MGMSG_PZ_REQ_OUTPUTPOS(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s);
int MGMSG_PZ_GET_OUTPUTPOS(UCHAR *b, UCHAR &cid, double &percent, UCHAR &d, UCHAR &s);
int MGMSG_PZ_SET_OUTPUTPOS(UCHAR *b, UCHAR cid, double percent, UCHAR d, UCHAR s);

int MGMSG_PZ_REQ_OUTPUTMAXVOLTS(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s);
int MGMSG_PZ_GET_OUTPUTMAXVOLTS(UCHAR *b, UCHAR &cid, double &volt, UCHAR &d, UCHAR &s,double maxV);
int MGMSG_PZ_SET_OUTPUTMAXVOLTS(UCHAR *b, UCHAR cid, double volt, UCHAR d, UCHAR s);

int MGMSG_PZ_SET_TPZ_IOSETTINGS(UCHAR *b, UCHAR cid, double VoltageLimit, WORD HubAnalogInput, UCHAR d, UCHAR s);
int MGMSG_PZ_REQ_TPZ_IOSETTINGS(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s);
int MGMSG_PZ_GET_TPZ_IOSETTINGS(UCHAR *b, UCHAR &cid, double &VoltageLimit, WORD &HubAnalogInput, UCHAR &d, UCHAR &s);

int MGMSG_PZ_SET_INPUTVOLTSOURCE(UCHAR *b, UCHAR cid, UCHAR mode, UCHAR d, UCHAR s);
int MGMSG_PZ_REQ_INPUTVOLTSOURCE(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s);
int MGMSG_PZ_GET_INPUTVOLTSOURCE(UCHAR *b, UCHAR &cid, UCHAR &mode, UCHAR &d, UCHAR &s);

int MGMSG_PZ_SET_PICONSTS(UCHAR *b, UCHAR cid, int prop,int integral, UCHAR d, UCHAR s);
int MGMSG_PZ_REQ_PICONSTS(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s);
int MGMSG_PZ_GET_PICONSTS(UCHAR *b, UCHAR &cid, int& prop,int &integral, UCHAR &d, UCHAR &s);

int MGMSG_PZ_SET_DISPSETTINGS(UCHAR *b, int disIntensity, UCHAR d, UCHAR s);
int MGMSG_PZ_REQ_DISPSETTINGS(UCHAR *b,UCHAR d, UCHAR s);
int MGMSG_PZ_GET_DISPSETTINGS(UCHAR *b, int& disIntensity,UCHAR &d, UCHAR &s);

int MGMSG_MOT_ACK_DCSTATUSUPDATE(UCHAR *b, UCHAR d, UCHAR s);
#endif //GENERIC_INSTRUMENT_H
