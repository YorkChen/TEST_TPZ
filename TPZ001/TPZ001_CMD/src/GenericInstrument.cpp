
/************************************************************************/
// FILE NAME :		GenericInstrument.cpp
// VERSION :	
// DATE :			2013. 01. 22
// COMPANY :		thorlabs
// DESCRIPTION :   
//		v 0.1 : 2013. 01. 22 start project
// Author :		eason dai(edai@thorlabs.com)
//                                                                 
/************************************************************************/
#include <string.h>
#include "GenericInstrument.h"
#include "apt_convert.h"

using namespace APT_TPZ001;

int MGMSG_HW_REQ_INFO(UCHAR *b,UCHAR d,UCHAR s)
{
    if(!b) return -1;
    b[0]=0x05;b[1]=0x00;b[2]=0x00;b[3]=0x00;b[4]=d;b[5]=s;return 6;
}

int MGMSG_HW_START_UPDATEMSGS(UCHAR *b,UCHAR updaterate,UCHAR unused,
                              UCHAR d,UCHAR s)
{
    if(!b) return -1;
    b[0]=0x11;b[1]=0x00;b[2]=updaterate;b[3]=unused;b[4]=d;b[5]=s;return 6;
}

int MGMSG_MOD_IDENTIFY(UCHAR *b, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x23;b[1]=0x02;b[2]=0x00;b[3]=0x00;b[4]=d;b[5]=s;return 6;
}

int MGMSG_HW_RESPONSE(UCHAR *b, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x80;b[1]=0x00;b[2]=0x00;b[3]=0x00;b[4]=d;b[5]=s;return 6;
}

int MGMSG_HW_STOP_UPDATEMSGS(UCHAR *b, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x12;b[1]=0x00;b[2]=0x00;b[3]=0x00;b[4]=d;b[5]=s;return 6;
}

int MGMSG_HW_DISCONNECT(UCHAR *b, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x02;b[1]=0x00;b[2]=0x00;b[3]=0x00;b[4]=d;b[5]=s;return 6;
}

int MGMSG_HUB_REQ_BAYUSED(UCHAR *b, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x65;b[1]=0x00;b[2]=0x00;b[3]=0x00;b[4]=d;b[5]=s;return 6;
}

void MGMSG_HW_GET_INFO(CHAR *Msg_Data, UCHAR &d, UCHAR &s, LONG &serial_number, CHAR *mode_number,
                       WORD &type, BYTE *firmware_version, CHAR *notes, BYTE* empty_space, WORD &hw_version, WORD &mode_state, WORD &nchs)
{
    d = Msg_Data[4];
    s = Msg_Data[5];
    serial_number = CharFormat2Long(&Msg_Data[6]);
    memcpy(mode_number,(Msg_Data+10), 8);
    type = CharFormat2Word(&Msg_Data[18]);
    memcpy(firmware_version,(Msg_Data+20), 4);
    memcpy(notes,(Msg_Data+24), 48);
    memcpy(empty_space,(Msg_Data+72),12);
    hw_version = CharFormat2Word(&Msg_Data[84]);
    mode_state = CharFormat2Word(&Msg_Data[86]);
    nchs = CharFormat2Word(&Msg_Data[88]);
}

void MGMSG_HW_RICHRESPONSE(CHAR *Msg_Data, UCHAR &d, UCHAR &s, WORD &msgident,WORD &code, CHAR *notes)
{
    d = Msg_Data[4];
    s = Msg_Data[5];
    msgident = CharFormat2Word(&Msg_Data[6]);
    code = CharFormat2Word(&Msg_Data[8]);
    memcpy(notes,(Msg_Data+10), 64);
}

void MGMSG_HUB_GET_BAYUSED(CHAR *Msg_Data, UCHAR &bay_ident, UCHAR &d, UCHAR &s)
{
    bay_ident = Msg_Data[2];
    d = Msg_Data[4];
    s = Msg_Data[5];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// MGMSG_PZ
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///

int MGMSG_PZ_SET_POSCONTROLMODE(UCHAR *b, UCHAR cid, UCHAR mode, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x40;b[1]=0x06;b[2]=cid;b[3]=mode;b[4]=d;b[5]=s;return 6;
}

int MGMSG_PZ_REQ_POSCONTROLMODE(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x41;b[1]=0x06;b[2]=cid;b[3]=0;b[4]=d;b[5]=s;return 6;
}

int MGMSG_PZ_GET_POSCONTROLMODE(UCHAR *b, UCHAR &cid, UCHAR &mode, UCHAR &d, UCHAR &s)
{
    if(!b) return -1;
    if(b[0]==0x42&&b[1]==0x06) {
        cid = b[2];
        mode = b[3];
        d = b[4];
        s = b[5];
        return 0;
    }
    else return -1;
}

int MGMSG_PZ_SET_OUTPUTVOLTS(UCHAR *b, UCHAR cid, double volt, UCHAR d, UCHAR s,double maxV)
{
    short *v=(short *)&b[8];
    if(!b) return -1;
    b[0]=0x43;b[1]=0x06;b[2]=0x04;b[3]=00;b[4]=d;b[5]=s;
    b[6]=cid; b[7]=0;
    *v = TPZ001_V2Word(volt,maxV);
    return 10;
}

int MGMSG_PZ_REQ_OUTPUTVOLTS(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x44;b[1]=0x06;b[2]=cid;b[3]=0;b[4]=d;b[5]=s;return 6;
}

int MGMSG_PZ_GET_OUTPUTVOLTS(UCHAR *b, UCHAR &cid, double &volt, UCHAR &d, UCHAR &s,double maxV)
{
    if(!b) return -1;
    short *v=(short *)&b[8];
    if(b[0]==0x45&&b[1]==0x06&&b[2]==0x04&&b[3]==0x00) {
        d = b[4];
        s = b[5];
        cid = b[6];
        volt = TPZ001_Word2V(*v,maxV);
        return 0;
    }
    else return -1;
}

int MGMSG_PZ_REQ_OUTPUTPOS(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x47;b[1]=0x06;b[2]=cid;b[3]=0;b[4]=d;b[5]=s;return 6;
}

int MGMSG_PZ_GET_OUTPUTPOS(UCHAR *b, UCHAR &cid, double &percent, UCHAR &d, UCHAR &s)
{
    if(!b) return -1;
    unsigned short *v=(unsigned short *)&b[8];
    if(b[0]==0x48&&b[1]==0x06&&b[2]==0x04&&b[3]==0x00) {
        d = b[4];
        s = b[5];
        cid = b[6];
        percent = TPZ001_PosToPercent(*v);
        return 0;
    }
    else return -1;
}

int MGMSG_PZ_SET_OUTPUTPOS(UCHAR *b, UCHAR cid, double percent, UCHAR d, UCHAR s)
{
    percent/=100;
    unsigned short *p=(unsigned short *)&b[8];
    if(!b) return -1;
    b[0]=0x46;b[1]=0x06;b[2]=0x04;b[3]=00;b[4]=d;b[5]=s;
    b[6]=cid; b[7]=0;
    *p = TPZ001_PercentToPos(percent);
    return 10;
}

int MGMSG_PZ_REQ_OUTPUTMAXVOLTS(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0xd5;b[1]=0x07;b[2]=cid;b[3]=0;b[4]=d;b[5]=s;return 6;
}

int MGMSG_PZ_GET_OUTPUTMAXVOLTS(UCHAR *b, UCHAR &cid, double &volt, UCHAR &d, UCHAR &s,double maxV)
{
    if(!b) return -1;
    short *v=(short *)&b[8];
    if(b[0]==0x82&&b[1]==0x06&&b[2]==0x06&&b[3]==0x00) {
        d = b[4];
        s = b[5];
        cid = b[6];
        volt = TPZ001_Word2V(*v,maxV);
        return 0;
    }
    else return -1;
}

int MGMSG_PZ_SET_OUTPUTMAXVOLTS(UCHAR *b, UCHAR cid, double volt, UCHAR d, UCHAR s,double maxV)
{
    if(!b) return -1;
    short *v=(short *)&b[8];
    if(!b) return -1;
    b[0]=0x82;b[1]=0x06;b[2]=0x06;b[3]=00;b[4]=d;b[5]=s;
    b[6]=cid; b[7]=0;
    *v = TPZ001_V2Word(volt,maxV);
    b[10]=0x001;b[11]=0x00;
    return 10;
}

int MGMSG_PZ_SET_TPZ_IOSETTINGS(UCHAR *b, UCHAR cid, double voltageLimit, WORD HubAnalogInput, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0xd4;b[1]=0x07;b[2]=0x0a;b[3]=00;b[4]=d;b[5]=s;
    b[6]=cid; b[7]=0;
    short vl =TPZ001_MaxV2Word(voltageLimit);
    b[8]=((CHAR*)&vl)[0]; b[9]=((CHAR*)&vl)[1];
    b[10]=((CHAR*)&HubAnalogInput)[0]; b[11]=((CHAR*)&HubAnalogInput)[1];
    b[12]=0; b[13]=0;b[14]=cid; b[15]=0;

//    TPZ001_SetMaxV(TPZ001_Word2MaxV(vl));
    return 16;
}

int MGMSG_PZ_REQ_TPZ_IOSETTINGS(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0xd5;b[1]=0x07;b[2]=cid;b[3]=0;b[4]=d;b[5]=s;return 6;
}

int MGMSG_PZ_GET_TPZ_IOSETTINGS(UCHAR *b, UCHAR &cid, double &VoltageLimit, WORD &HubAnalogInput, UCHAR &d, UCHAR &s)
{
    if(!b) return -1;
    if(b[0]==0xd6&&b[1]==0x07&&b[2]==0x0a&&b[3]==0x00) {
        d = b[4];
        s = b[5];
        cid = b[6];

        short *maxV=(short *)&b[8];
        VoltageLimit = TPZ001_Word2MaxV(*maxV);//(((WORD)b[9])<<8)|b[8];
        HubAnalogInput = (((WORD)b[11])<<8)|b[10];
        return 0;
    }
    else return -1;
}

int MGMSG_PZ_SET_INPUTVOLTSOURCE(UCHAR *b, UCHAR cid, UCHAR mode, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x52;b[1]=0x06;b[2]=0x04;b[3]=0;b[4]=d;b[5]=s;
    b[6]=cid;b[7]=0;b[8]=mode;b[9]=0;
    return 10;
}

int MGMSG_PZ_REQ_INPUTVOLTSOURCE(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x53;b[1]=0x06;b[2]=cid;b[3]=0;b[4]=d;b[5]=s;return 6;
}

int MGMSG_PZ_GET_INPUTVOLTSOURCE(UCHAR *b, UCHAR &cid, UCHAR &mode, UCHAR &d, UCHAR &s)
{
    if(!b) return -1;
    if(b[0]==0x54&&b[1]==0x06&&b[2]==0x04&&b[3]==0x00) {
        d = b[4];
        s = b[5];
        cid = b[6];
        mode = b[8];
        return 0;
    }
    else return -1;
}

int MGMSG_PZ_SET_PICONSTS(UCHAR *b, UCHAR cid, int prop,int integral, UCHAR d, UCHAR s)
{
    if(!b) return -1;

    b[0]=0x55;b[1]=0x06;b[2]=0x06;b[3]=0;b[4]=d;b[5]=s;
    b[6]=cid;b[7]=0;
    unsigned short* bp = (unsigned short*)&b[8];
    *bp =(unsigned short)prop;
    unsigned short* bi = (unsigned short*)&b[10];
    *bi = (unsigned short)integral;
    return 12;
}

int MGMSG_PZ_REQ_PICONSTS(UCHAR *b, UCHAR cid, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x56;b[1]=0x06;b[2]=cid;b[3]=0;b[4]=d;b[5]=s;return 6;
}

int MGMSG_PZ_GET_PICONSTS(UCHAR *b, UCHAR &cid, int& prop,int &integral, UCHAR &d, UCHAR &s)
{
    if(!b) return -1;
    if(b[0]==0x57&&b[1]==0x06&&b[2]==0x06&&b[3]==0x00) {
        d = b[4];
        s = b[5];
        cid = b[6];
        unsigned short* bp = (unsigned short*)&b[8];
        prop = *bp;
        unsigned short* bi = (unsigned short*)&b[10];
        integral = *bi;
        return 0;
    }
    else return -1;
}

int MGMSG_PZ_SET_DISPSETTINGS(UCHAR *b, int disIntensity, UCHAR d, UCHAR s)
{
    if(!b) return -1;

    b[0]=0xD1;b[1]=0x07;b[2]=0x02;b[3]=0;b[4]=d;b[5]=s;
    unsigned short* bp = (unsigned short*)&b[6];
    *bp =(unsigned short)disIntensity;
    return 8;
}

int MGMSG_PZ_REQ_DISPSETTINGS(UCHAR *b,UCHAR d, UCHAR s)
{
    if(!b) return -1;

    b[0]=0xd2;b[1]=0x07;b[2]=0x01;b[3]=0;b[4]=d;b[5]=s;return 6;
}

int MGMSG_PZ_GET_DISPSETTINGS(UCHAR *b, int& disIntensity,UCHAR &d, UCHAR &s)
{
    if(!b) return -1;
    if(b[0]==0xD3&&b[1]==0x07&&b[2]==0x02&&b[3]==0x00) {
        d = b[4];
        s = b[5];
        unsigned short* bp = (unsigned short*)&b[6];
        disIntensity = *bp;
        return 0;
    }
    else return -1;
}
int MGMSG_MOT_ACK_DCSTATUSUPDATE(UCHAR *b, UCHAR d, UCHAR s)
{
    if(!b) return -1;
    b[0]=0x92;b[1]=0x04;b[2]=0x00;b[3]=0x00;b[4]=d;b[5]=s;return 6;
}
