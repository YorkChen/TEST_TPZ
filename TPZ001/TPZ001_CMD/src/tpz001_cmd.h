/************************************************************************/
// FILE NAME: tpz001_cmd.h
// DATE:      2014. 03. 21
// COMPANY:		THORLABS
// Author:    eason dai
// DESCRIPTION: Defines funcions of interacting with device
/************************************************************************/
#ifndef TPZ001_CMD_H
#define TPZ001_CMD_H

#include "apt_type.h"

#if defined(TPZ001_CMD_LIBRARY)
#  define TPZ001_CMDSHARED_EXPORT  extern "C" __declspec(dllexport)
#else
#  define TPZ001_CMDSHARED_EXPORT extern "C" __declspec(dllimport)
#endif

/// <summary>
/// InitDevice function. Load and Check UART_LIBRARY dll
/// </summary>
/// <param name="path">set path of UART_LIBRARY dll</param>
/// <returns> non-negtive number: successfully; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_InitDevice(char *path,char* name);
/// <summary>
/// Release function. Free the loaded library
/// </summary>
/// <param name="path">set path of UART_LIBRARY dll</param>
/// <returns> not zero: successfully; zero : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_ReleaseDevice(char *path);
/// <summary>
/// OpenDevice function.
/// </summary>
/// <param name="sn">set serial number string</param>
/// <param name="nbaud">Normally 115200</param>
/// <param name="timeout">set timeout value in (s)</param>
/// <returns> non-negtive number: hdl number returned successfully; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_OpenDevice(char* sn, int nbaud, int timeout);

/// <summary>
/// CloseDevice function.
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <returns> 0: successfully; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_CloseDevice(int hDevice);
/// <summary>
/// list all the possible port on this computer.
/// </summary>
/// <param name="nPort">port list returned string include serial number and device descriptor, seperated by comma</param>
/// <param name="var">max length value of nPort buffer</param>
/// <returns>non-negtive number: number of device in the list; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_GetDeviceList(char *nPort, int var);
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_ReadDataType(int hDevice, RESPONSE_MSG *msg);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_ReadData(int hDevice, char *oBuf, unsigned short type, int size);


/// <summary>
/// This function is called on start up to notify the controller of the source and destination addresses.
/// A client application must call this function as part of its initialization process.
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_StartCtrl(int hDevice);
/// <summary>
/// Instruct hardware unit to identify itself (by flashing its front panel LEDs).
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Identify(int hDevice);
/// <summary>
/// Used to get the output voltage applied to the piezo actuator.
/// This command is applicable only in Open Loop mode.
/// If called when in Closed Loop mode it is ignored.
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_Volts(int hDevice);
/// <summary>
/// Used to get the output position of piezo actuator.
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_Pos(int hDevice);

/// <summary>
/// Set Voltage function.
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <param name="volt">voltage in double</param>
/// <returns> 0: successfully; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_Volts(int hDevice,double volt);
/// <summary>
/// Used to set the output position of piezo actuator.
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <param name="percent">The output position of the piezo relative to the zero position.</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_Pos(int hDevice,double percent);
/// <summary>
/// This method sets the control loop status
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <param name="mode">The Control Mode is specified by this parameter</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_ControlMode(int hDevice,CONTROL_MODE mode);
/// <summary>
/// This method gets the control loop status
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_ControlMode(int hDevice);
/// <summary>
/// This function is used to get various I/O settings
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_IOSettings(int hDevice);
/// <summary>
/// This function is used to set various I/O settings
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <param name="voltMax">This parameter sets the maximum output</param>
/// <param name="hubAnalog">This parameter is used to select the way in which the feedback signal is routed to the Piezo unit</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_IOSettings(int hDevice,double voltMax,unsigned short hubAnalog);
/// <summary>
/// Used to set the input source(s) which controls the output from the HV amplifier circuit
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <param name="mode">select the various analog sources</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_InputSource(int hDevice,INPUTSRC_MODE mode);
/// <summary>
/// Used to get the input source(s) which controls the output from the HV amplifier circuit
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_InputSource(int hDevice);
/// <summary>
/// Used to set the proportional and integration feedback loop constants.
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <param name="prop">The value of the proportional term in the range 0 to 255.</param>
/// <param name="integral">The value of the Integral term.in the range 0 to 255</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_PIConsts(int hDevice,int prop,int integral);
/// <summary>
/// Used to get the proportional and integration feedback loop constants.
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_PIConsts(int hDevice);
/// <summary>
/// Used to set the intensity of the LED display on the front of the TPZ unit.
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <param name="intensity">The intensity is set as a value from 0 (Off) to 255 (brightest).</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_DISPIntensity(int hDevice,int intensity);
/// <summary>
/// Used to get the intensity of the LED display on the front of the TPZ unit.
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_DISPIntensity(int hDevice);

/// <summary>
/// Used to tell the device controller is alive.
/// </summary>
/// <param name="hDevice">handle returned from open function</param>
/// <returns>0: Successful; negtive number : failed.</returns>
TPZ001_CMDSHARED_EXPORT int fnTDC001_DLL_Ack_DCStatusUpdate(int hDevice);
#endif // TPZ001_CMD_H
