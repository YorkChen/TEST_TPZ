#include "apt_handler.h"
#include "GenericInstrument.h"
#include "apt_convert.h"
#include "memory.h"
using namespace APT_TPZ001;

#define TEXT_READ_BUF 1024

AptHandler::AptHandler()
{
    nPortHandle = -1;
}

AptHandler::AptHandler(UART_LIBRARY *u, char *serial_number, int nbaud, int timeout)
{
    nPortHandle = -1;
    init_uart_library(u);
    init_device(serial_number, nbaud, timeout);
}

AptHandler::~AptHandler()
{
    release_device();
}

int AptHandler::init_uart_library(UART_LIBRARY *u)
{
    uu = NULL;
    if(u != NULL) {
        if(!u->open) return -2;
        if(!u->isOpen) return -3;
        if(!u->list) return -4;
        if(!u->close) return -5;
        if(!u->write) return -6;
        if(!u->read) return -7;
        if(!u->timeout) return -8;
        if(!u->Purge) return -9;
    }
    else {
        return -1;
    }
    uu = u;
    return 0;
}

int AptHandler::init_device(char *serial_number, int nbaud, int timeout)
{
    if(uu == NULL ) return -1;
    else {
        nPortHandle = uu->open(serial_number, nbaud, timeout);
        if(nPortHandle < 0) return -2;
        else {
            readBuf = new CirBuf<char>(TEXT_READ_BUF);
            if(readBuf == NULL) return -3;
            return 0;
        }
    }
}

int AptHandler::release_device()
{
    if(uu == NULL ) return -1;
    else {
        if(nPortHandle < 0) return -2;
        else {
            delete readBuf;
            uu->close(nPortHandle);
            nPortHandle = -1;
            return 0;
        }
    }
}

int AptHandler::send_command(char * MsgData, unsigned int MsgSize)
{
    if(uu == NULL ) return -1;
    else {
        if(nPortHandle < 0) return -2;
        else {
            return uu->write(nPortHandle,MsgData,MsgSize);
        }
    }
}

int AptHandler::get_DataPacket_Size(char * msgHeader)
{
    if(((unsigned short) msgHeader[4]&0x80)!=0x80)
    {
        return 0;
    }
    unsigned short part1 =(unsigned short)msgHeader[3];
    return (unsigned short)msgHeader[2]+(part1<<8);
}

int AptHandler::read_data_type(int thread_hold, RESPONSE_MSG *msg)
{
    int dwBytes = 0,r;
    char tBuf[TEXT_READ_BUF/2];
    int limit = ((thread_hold==1) ? -1: 1)*(TEXT_READ_BUF/2);
    if(uu == NULL ) return -2;
    else {
        if(nPortHandle < 0) return -3;
        else {
            dwBytes = uu->read(nPortHandle,tBuf, limit);
            if(dwBytes < 0) return -7;
            else if(dwBytes > 0) {
                r = readBuf->push(tBuf,dwBytes);
                if(r < 0) return -4;
            }
            dwBytes = readBuf->size();
            if(dwBytes < 6) {
                msg->res_msg_type = APT_MGMSG_MOT_NOT_ENOUGH_LENGTH;
                msg->res_msg_size = APT_MGMSG_MOT_NOT_ENOUGH_LENGTH_SIZE;
                return 0;
            }
            else {
                unsigned short head;
                r = readBuf->pop(tBuf,6,0);
                if(r != 6) return -5;
                head = ((unsigned short)tBuf[1]<<8) + ((unsigned short)tBuf[0]&0x00ff);
                r = get_DataPacket_Size(tBuf)+6;
                if(r<0) return -6;
                if(dwBytes < r) {
                    msg->res_msg_type = APT_MGMSG_MOT_NOT_ENOUGH_LENGTH;
                    msg->res_msg_size = APT_MGMSG_MOT_NOT_ENOUGH_LENGTH_SIZE;
                    return 0;
                }
                else {
                    msg->res_msg_type = (APT_RESPONSE_MESSAGE_TYPE)head;
                    msg->res_msg_size = r;
                    return 1;
                }
            }
        }
    }
}

int AptHandler::read_data(char *oBuf,unsigned short type, int size)
{
    int r;
    char tBuf[TEXT_READ_BUF/2];
    unsigned short head;
    r = readBuf->pop(tBuf,size,1);
    if(r <0) return -1;
    head = ((unsigned short)tBuf[1]<<8) + ((unsigned short)tBuf[0]&0x00ff);
    if(head != type) return -2;
    else
    {
        APT_RESPONSE_MESSAGE_TYPE res_type = (APT_RESPONSE_MESSAGE_TYPE)type;
        switch(res_type)
        {
        case APT_MGMSG_PZ_GET_OUTPUTVOLTS:
        {
            APT_MGMSG_PZ_GET_OUTPUTVOLTS_T apt_data;
            MGMSG_PZ_GET_OUTPUTVOLTS((UCHAR*)tBuf,apt_data.cid,apt_data.volts,apt_data.d,apt_data.s,m_VolScaler);
            r = sizeof(apt_data);
            memcpy(oBuf,&apt_data, r);
            break;
        }
        case APT_MGMSG_PZ_GET_OUTPUTPOS:
        {
            APT_MGMSG_PZ_GET_OUTPUTPOS_T apt_data;
            MGMSG_PZ_GET_OUTPUTPOS((UCHAR*)tBuf,apt_data.cid,apt_data.percent,apt_data.d,apt_data.s);
            r = sizeof(apt_data);
            memcpy(oBuf,&apt_data, r);
            break;
        }
        case APT_MGMSG_PZ_GET_POSCONTROLMODE:
        {
            APT_MGMSG_PZ_GET_CONTROLMODE_T apt_data;
            MGMSG_PZ_GET_POSCONTROLMODE((UCHAR*)tBuf,apt_data.cid,apt_data.mode,apt_data.d,apt_data.s);
            r = sizeof(apt_data);
            memcpy(oBuf,&apt_data, r);
            break;
        }
        case APT_MGMSG_PZ_GET_IOSETTINGS:
        {
            APT_MGMSG_PZ_GET_IOSETTINGS_T apt_data;
            MGMSG_PZ_GET_TPZ_IOSETTINGS((UCHAR*)tBuf, apt_data.cid, apt_data.maxVolt, apt_data.hubAnalog, apt_data.d, apt_data.s);
            SetVScaler(apt_data.maxVolt);
            r = sizeof(apt_data);
            memcpy(oBuf,&apt_data, r);
            break;
        }
        case APT_MGMSG_PZ_GET_INPUTVOLTSSRC:
        {
            APT_MGMSG_PZ_GET_INPUTVOLTSOURCE_T apt_data;
            MGMSG_PZ_GET_INPUTVOLTSOURCE((UCHAR*)tBuf,apt_data.cid,apt_data.mode,apt_data.d,apt_data.s);
            r = sizeof(apt_data);
            memcpy(oBuf,&apt_data, r);
            break;
        }
        case APT_MGMSG_PZ_GET_PICONSTS:
        {
            APT_MGMSG_PZ_GET_PICONSTS_T apt_data;
            MGMSG_PZ_GET_PICONSTS((UCHAR*)tBuf,apt_data.cid,apt_data.prop,apt_data.integral,apt_data.d,apt_data.s);
            r = sizeof(apt_data);
            memcpy(oBuf,&apt_data, r);
            break;
        }
        case APT_MGMSG_PZ_GET_TPZ_DISPSETTINGS:
        {
            APT_MGMSG_PZ_GET_DISPSETTING_T apt_data;
            MGMSG_PZ_GET_DISPSETTINGS((UCHAR*)tBuf,apt_data.intensity,apt_data.d,apt_data.s);
            r = sizeof(apt_data);
            memcpy(oBuf,&apt_data, r);
            break;
        }
        case APT_MGMSG_HW_GET_INFO:
        {
            APT_MGMSG_HW_GET_INFO_T apt_data;
            MGMSG_HW_GET_INFO(tBuf, apt_data.d, apt_data.s, apt_data.serial_number, apt_data.mode_number,
                              apt_data.type,apt_data.firmware_version, apt_data.notes, apt_data.empty_space,
                              apt_data.hw_version, apt_data.mode_state, apt_data.nchs);
            r = sizeof(apt_data);
            memcpy(oBuf,&apt_data, r);
            unsigned short id = ((apt_data.empty_space[10]&0x00ff) + ((apt_data.empty_space[11]<<8)&0xff00))-1;
            break;
        }
        default :
        {
            r = -3;
        }
        }
        return r;
    }
}
