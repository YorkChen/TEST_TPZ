#include "tpz001_cmd.h"
#include <Windows.h>
#include <vector>
#include "GenericInstrument.h"
#include "apt_handler.h"
#include "apt_convert.h"
#include"tchar.h"
const UCHAR Channel_ID = 0x01;
const UCHAR Parameter_D_1 = 0x50;
const UCHAR Parameter_D_2 = 0xD0;
const UCHAR Parameter_S = 0x01;

#define  MAX_BUF_SIZE 32
using namespace std;
static UART_LIBRARY fnUART_LIBRARY;
static vector<AptHandler *> vecAptHandler;
HMODULE dll_handle=0;



TPZ001_CMDSHARED_EXPORT int  fnTPZ001_DLL_InitDevice(char *path,char* name)
{
    if(dll_handle!=0)
    {
        return 0;
    }
    memset(&fnUART_LIBRARY,0, sizeof(UART_LIBRARY));

#ifndef UNDER_CE
    if(path != NULL)
    {
       SetCurrentDirectoryA(path);
    }

    WCHAR   wname[MAX_PATH]={0};
    MultiByteToWideChar(CP_ACP,   0,  name,  -1,   wname,   sizeof(wname));
    dll_handle = LoadLibrary(name);

    if(dll_handle)
    {
        fnUART_LIBRARY.open=(fnUART_open)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_open"));
        if(!fnUART_LIBRARY.open) return -2;
        fnUART_LIBRARY.isOpen=(fnUART_isOpen)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_isOpen"));
        if(!fnUART_LIBRARY.isOpen) return -3;
        fnUART_LIBRARY.list=(fnUART_list)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_list"));
        if(!fnUART_LIBRARY.list) return -4;
        fnUART_LIBRARY.close=(fnUART_close)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_close"));
        if(!fnUART_LIBRARY.close) return -5;
        fnUART_LIBRARY.write=(fnUART_write)GetProcAddress(dll_handle,_T("fnUART_LIBRARY_write"));
        if(!fnUART_LIBRARY.write) return -6;
        fnUART_LIBRARY.read=(fnUART_read)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_read"));
        if(!fnUART_LIBRARY.read) return -7;
        fnUART_LIBRARY.timeout=(fnUART_timeout)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_timeout"));
        if(!fnUART_LIBRARY.timeout) return -8;
        fnUART_LIBRARY.Purge=(fnUART_Purge)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_Purge"));
        if(!fnUART_LIBRARY.Purge) return -9;
        return 0;
    }
    else return -1;
#else
    dll_handle = LoadLibrary(TEXT("uart_library_ftdi.dll"));

    if(dll_handle)
    {
        fnUART_LIBRARY.open=(fnUART_open)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_open"));
        if(!fnUART_LIBRARY.open) return -2;
        fnUART_LIBRARY.isOpen=(fnUART_isOpen)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_isOpen"));
        if(!fnUART_LIBRARY.isOpen) return -3;
        fnUART_LIBRARY.list=(fnUART_list)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_list"));
        if(!fnUART_LIBRARY.list) return -4;
        fnUART_LIBRARY.close=(fnUART_close)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_close"));
        if(!fnUART_LIBRARY.close) return -5;
        fnUART_LIBRARY.write=(fnUART_write)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_write"));
        if(!fnUART_LIBRARY.write) return -6;
        fnUART_LIBRARY.read=(fnUART_read)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_read"));
        if(!fnUART_LIBRARY.read) return -7;
        fnUART_LIBRARY.timeout=(fnUART_timeout)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_timeout"));
        if(!fnUART_LIBRARY.timeout) return -8;
        fnUART_LIBRARY.Purge=(fnUART_Purge)GetProcAddress(dll_handle, _T("fnUART_LIBRARY_Purge"));
        if(!fnUART_LIBRARY.Purge) return -9;
        return 0;
    }
    else return -1;
#endif
}
TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_ReleaseDevice(char * path)
{
#ifndef UNDER_CE
    if(path != NULL)
    {
        SetCurrentDirectoryA(path);
    }
#endif
    if(dll_handle==0)
    {
        return 0;
    }
    return FreeLibrary(dll_handle);
}

TPZ001_CMDSHARED_EXPORT int  fnTPZ001_DLL_OpenDevice(char* sn, int nbaud, int timeout)
{
    int ret=0;
    AptHandler *a = new AptHandler;
    {
        a->init_uart_library(&fnUART_LIBRARY);
        ret = a->init_device(sn,nbaud,timeout);
        if(ret < 0) return -2;
        else {
            vecAptHandler.push_back(a);
            ret = vecAptHandler.size()-1;
            return ret;
        }
    }
}

TPZ001_CMDSHARED_EXPORT int  fnTPZ001_DLL_CloseDevice(int hDevice)
{
    void *p = vecAptHandler[hDevice];
    if(vecAptHandler[hDevice] != 0) {
        vecAptHandler[hDevice]->release_device();
        delete p;
        vecAptHandler[hDevice] = 0;
    }
    return 0;
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_GetDeviceList(char *nPort, int var)
{
    return fnUART_LIBRARY.list(nPort, var);
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_ReadDataType(int hDevice, RESPONSE_MSG *msg)
{
    int ret;
    if (hDevice < 0) return -1;
    else{
        ret = vecAptHandler[hDevice]->read_data_type(0,msg);
        return ret < 0 ? -2: ret;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_StartCtrl(int hDevice)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = 12,ret;
        msg_buf[0]=0x18; msg_buf[1]=0x00; msg_buf[2]=0x00; msg_buf[3]=0x00;
        msg_buf[4]=Parameter_D_1; msg_buf[5]=Parameter_S;
        MGMSG_HW_REQ_INFO((UCHAR*)(msg_buf+6),Parameter_D_1,Parameter_S);
        ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_ReadData(int hDevice, char *oBuf, unsigned short type, int size)
{
    int ret;
    if (hDevice < 0) return -1;
    else{
        ret = vecAptHandler[hDevice]->read_data(oBuf,type,size);
        return ret < 0 ? -2:ret;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_Volts(int hDevice)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_REQ_OUTPUTVOLTS((UCHAR*)msg_buf, Channel_ID,
                                                Parameter_D_1, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_Volts(int hDevice,double volt)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_SET_OUTPUTVOLTS((UCHAR*)msg_buf, Channel_ID,
                                                volt, Parameter_D_2, Parameter_S,vecAptHandler[hDevice]->GetVScaler());
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}


TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_Pos(int hDevice)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_REQ_OUTPUTPOS((UCHAR*)msg_buf, Channel_ID,
                                              Parameter_D_1, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_Pos(int hDevice,double percent)
{
    if (hDevice < 0) return -1;
    else{
        percent=(percent-APT_TPZ001::B)/APT_TPZ001::K;
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_SET_OUTPUTPOS((UCHAR*)msg_buf, Channel_ID,
                                              percent, Parameter_D_2, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Identify(int hDevice)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_MOD_IDENTIFY((UCHAR*)msg_buf,
                                          Parameter_D_1, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_ControlMode(int hDevice,CONTROL_MODE mode)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_SET_POSCONTROLMODE((UCHAR*)msg_buf, Channel_ID,
                                                   (UCHAR)mode, Parameter_D_1, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_ControlMode(int hDevice)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_REQ_POSCONTROLMODE((UCHAR*)msg_buf, Channel_ID,
                                                   Parameter_D_1, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_IOSettings(int hDevice)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_REQ_TPZ_IOSETTINGS((UCHAR*)msg_buf, Channel_ID,
                                                   Parameter_D_1, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_IOSettings(int hDevice,double voltMax,unsigned short hubAnalog)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_SET_TPZ_IOSETTINGS((UCHAR*)msg_buf, Channel_ID,
                                                   voltMax, hubAnalog, Parameter_D_2, Parameter_S);
        vecAptHandler[hDevice]->SetVScaler(voltMax);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_InputSource(int hDevice,INPUTSRC_MODE mode)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_SET_INPUTVOLTSOURCE((UCHAR*)msg_buf, Channel_ID,
                                                    (UCHAR)mode, Parameter_D_2, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_InputSource(int hDevice)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_REQ_INPUTVOLTSOURCE((UCHAR*)msg_buf, Channel_ID,
                                                    Parameter_D_1, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_PIConsts(int hDevice,int prop,int integral)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_SET_PICONSTS((UCHAR*)msg_buf, Channel_ID,
                                             prop, integral, Parameter_D_2, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_PIConsts(int hDevice)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_REQ_PICONSTS((UCHAR*)msg_buf, Channel_ID,
                                             Parameter_D_1, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Set_DISPIntensity(int hDevice,int intensity)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_SET_DISPSETTINGS((UCHAR*)msg_buf,
                                                 intensity, Parameter_D_2, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}

TPZ001_CMDSHARED_EXPORT int fnTPZ001_DLL_Req_DISPIntensity(int hDevice)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_PZ_REQ_DISPSETTINGS((UCHAR*)msg_buf, Parameter_D_1, Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}
TPZ001_CMDSHARED_EXPORT int fnTDC001_DLL_Ack_DCStatusUpdate(int hDevice)
{
    if (hDevice < 0) return -1;
    else{
        char msg_buf[MAX_BUF_SIZE];
        int buf_size = MGMSG_MOT_ACK_DCSTATUSUPDATE((UCHAR*)msg_buf,
            Parameter_D_1,Parameter_S);
        int ret = vecAptHandler[hDevice]->send_command(msg_buf, buf_size);
        return ret < 0 ? -2:0;
    }
}
