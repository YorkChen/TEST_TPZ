/************************************************************************/
// FILE NAME: apt_handler.h
// DATE:      2014. 03. 21
// COMPANY:		THORLABS
// Author:    eason dai
// DESCRIPTION: Defines apt handler functions
/************************************************************************/
#ifndef APT_HANDLER_H
#define APT_HANDLER_H
#include "cir_buf.h"
#include "apt_type.h"

typedef int 	(*fnUART_open	)(char *sn, int nBaud, int timeout);
typedef int 	(*fnUART_isOpen	)(int hdl);
typedef int 	(*fnUART_list	)(char *nPort, int var);
typedef int 	(*fnUART_close	)(int hdl);
typedef int 	(*fnUART_write	)(int hdl, char *b, int size);
typedef int 	(*fnUART_read	)(int hdl, char *b, int limit);
typedef int 	(*fnUART_Set	)(int hdl, char *c,int var);
typedef int 	(*fnUART_Get	)(int hdl, char *c,char *d);
typedef int 	(*fnUART_Req	)(int hdl, char *c,char *d);
typedef void 	(*fnUART_timeout)(int hdl, int time);
typedef int 	(*fnUART_Purge	)(int hdl, int flag);

typedef struct
{
    fnUART_open open;
    fnUART_isOpen isOpen;
    fnUART_list list;
    fnUART_close close;
    fnUART_write write;
    fnUART_read read;
    fnUART_Set Set;
    fnUART_Get Get;
    fnUART_Req Req;
    fnUART_timeout timeout;
    fnUART_Purge Purge;
}UART_LIBRARY;

class AptHandler
{
public:
    AptHandler();
    AptHandler(UART_LIBRARY *u,char *serial_number, int nbaud, int timeout);
    ~AptHandler();
    int init_uart_library(UART_LIBRARY *u);
    int init_device(char *serial_number, int nbaud, int timeout);
    int send_command(char * MsgData, unsigned int MsgSize);
    int release_device();

    int read_data_type(int thread_hold, RESPONSE_MSG *msg);
    int read_data(char *oBuf,unsigned short type, int size);
    double GetVScaler(){return m_VolScaler;}
    void SetVScaler(double volt){
        if(volt==100.0)//the voltage range of tpz in 100v is as same as it in 150v.
            m_VolScaler= 150.0;
        else
            m_VolScaler= volt;
        }
private:    
    int get_DataPacket_Size(char *msgHeader);
    UART_LIBRARY *uu;
    int nPortHandle;
    CirBuf<char> *readBuf;
    double m_VolScaler;
};

#endif // APT_HANDLER_H
