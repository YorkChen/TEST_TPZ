#include "apt_convert.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// COMMON MISC
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static int apt_device_id;
int SetDeviceID(int id)
{
    apt_device_id = id;
    return 0;
}

int GetDeviceID(void)
{
    return apt_device_id;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// TPZ001
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
namespace APT_TPZ001 {

unsigned short CharFormat2Word(char* ch)
{
    return (unsigned short)((ch[0]&0x00ff) + ((ch[1]<<8)&0xff00));
}

long CharFormat2Long(char* ch)
{
    return (long)((ch[0]&0x000000ff)  + ((ch[1]<<8)&0x0000ff00)+((ch[2]<<16)&0x00ff0000)+((ch[3]<<24)&0xff000000));
}

//static double TPZ001_MaxV=75.0;
//void TPZ001_SetMaxV(double v)
//{
//    if(v==100.0)
//        TPZ001_MaxV =150.0;
//    else
//        TPZ001_MaxV=v;
//}

//double TPZ001_GetMaxV(double v)
//{
//    //the voltage range of tpz in 100v is as same as it in 150v.
//    if(v==100.0)
//        return 150.0;
//    else
//        return v;
//}

short TPZ001_V2Word(double v,double TPZ001_MaxV)
{
    short r=0;
    if(absv(v)<=TPZ001_MaxV){
        r = (short)(v*32767/TPZ001_MaxV);
    }
    return r;
}

double TPZ001_Word2V(short w,double TPZ001_MaxV)
{
    double v=w;
    v = v*(TPZ001_MaxV)/32767;
    return v;
}

double TPZ001_PosToPercent(int w)
{
    double v=w;
    v = v/65535;
    return v;
}

int TPZ001_PercentToPos(double p)
{
    int v = p*65535;
    return v;
}

short TPZ001_MaxV2Word(double v)
{
    short r=0x01;
    if(v==75.0)
        r = 0x01;
    else if(v==100.0)
        r=0x02;
    else if(v == 150.0)
        r= 0x03;
    return r;
}

double TPZ001_Word2MaxV(unsigned short v)
{
    double d=75;
    if(v==0x01)
        d = 75;
    else if(v==0x02)
        d =100;
    else if(v == 0x03)
        d= 150;
    return d;
}
}
