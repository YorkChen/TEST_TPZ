/************************************************************************/
// FILE NAME :
// VERSION :
// DATE :			2014. 03. 21
// COMPANY :		thorlab
// DESCRIPTION : Buffer operate class
//		v 0.1 : 2014.03.21 start project
//		v 0.2 :
//		v 0.3 :
//		v 1.0 :
// Author :		eason dai
// EMAIL : edai@thorlabs.com
/************************************************************************/

#ifndef CIR_BUF_H
#define CIR_BUF_H

template<class T> class CirBuf
{
public:
    CirBuf(int max) {
        _max = -1;
        init(max);
    }

    CirBuf() {
        _max = -1;
    }

    ~CirBuf()
    {
        delete p;
        wp = rp = p = 0;
    }

    void init(int max) {
        _max = max;
        p = new T[max];
        if(p){
            wp = rp = p;
        }
    }

    int size()
    {
        int r ;
        r = size2();
        return r;
    }

    bool isFull()
    {
        bool full = (size() == _max -1) ? 1:0;
        return full;
    }

    bool isEmpty()
    {
        bool empty = (size() == 0) ? 1:0;
        return empty;
    }

    void clean()
    {
        wp = rp = p;
    }

    int push(T *t, int n)
    {
        int r=0,remain,size_to_stop;
        remain = _max -1 - size2();
        if(remain >= n ) {
            size_to_stop = _max - (wp -p);
            if(size_to_stop > n) {
                memcpy(wp,t,sizeof(T)*n);
                wp+=n;
            }
            else {
                int tt = (n -size_to_stop);
                memcpy(wp,t,sizeof(T)*size_to_stop);
                if(tt) {
                    memcpy(p,t + size_to_stop,sizeof(T)*tt);
                }
                wp = p +tt;
            }
        }
        else {
            r= -1;
        }
        return r;
    }

    int pop(T *o, int n, int pop_flag)
    {
        int i=0,size_to_stop=_max - (rp -p);
        i= size2();
        i= (i>=n) ? n:i;
        if(size_to_stop > i) {
            memcpy(o,rp,sizeof(T)*i);
            if(pop_flag) rp+=i;
        }
        else {
            int tt = (i -size_to_stop);
            memcpy(o,rp,sizeof(T)*size_to_stop);
            if(tt) {
                memcpy(o + size_to_stop,p,sizeof(T)*tt);
            }
            if(pop_flag) rp = p +tt;
        }
        return i;
    }
private:
    __inline int size2()
    {
        if(wp >= rp) {
            return wp - rp;
        }
        else {
            return _max - (rp - wp);
        }
    }
private:
    T *p;
    T *wp;
    T *rp;
    int _max;
};
#endif // CIR_BUF_H
