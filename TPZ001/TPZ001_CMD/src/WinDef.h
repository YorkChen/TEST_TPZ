/************************************************************************/
// FILE NAME: WinDef.h
// DATE:      2014. 03. 21
// COMPANY:		THORLABS
// Author:    eason dai
// DESCRIPTION: Defines some types and some simple functions
/************************************************************************/
#ifndef _WINDEF_
#define _WINDEF_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef BASETYPES
#define BASETYPES

#define VOID void
typedef char CHAR;
typedef short SHORT;
typedef long LONG;

typedef unsigned long ULONG;
typedef ULONG *PULONG;
typedef unsigned short USHORT;
typedef USHORT *PUSHORT;
typedef unsigned char UCHAR;
typedef UCHAR *PUCHAR;
typedef char *PSZ;
#endif  /* !BASETYPES */

#define MAX_PATH          260

#ifndef NULL
#define NULL               0
#endif

#ifndef FALSE
#define FALSE               0
#endif

#ifndef TRUE
#define TRUE                1
#endif

typedef unsigned long       DWORD;
typedef int                 BOOL;
typedef unsigned char       BYTE;
typedef unsigned short      WORD;
typedef float               FLOAT;
typedef int                 INT;
typedef unsigned int        UINT;

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif


#ifndef absv
#define absv(a)            (((a) > (0)) ? (a) : (-a))
#endif


#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#define MAKEWORD(a, b)      ((WORD)(((BYTE)(((DWORD_PTR)(a)) & 0xff)) | ((WORD)((BYTE)(((DWORD_PTR)(b)) & 0xff))) << 8))
#define MAKELONG(a, b)      ((LONG)(((WORD)(((DWORD_PTR)(a)) & 0xffff)) | ((DWORD)((WORD)(((DWORD_PTR)(b)) & 0xffff))) << 16))
#define LOWORD(l)           ((WORD)(((DWORD_PTR)(l)) & 0xffff))
#define HIWORD(l)           ((WORD)((((DWORD_PTR)(l)) >> 16) & 0xffff))
#define LOBYTE(w)           ((BYTE)(((DWORD_PTR)(w)) & 0xff))
#define HIBYTE(w)           ((BYTE)((((DWORD_PTR)(w)) >> 8) & 0xff))

#ifdef __cplusplus
}
#endif
#endif /* _WINDEF_ */


