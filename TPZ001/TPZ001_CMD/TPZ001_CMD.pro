#-------------------------------------------------
#
# Project created by QtCreator 2014-05-08T15:51:53
#
#-------------------------------------------------

QT       -= gui core

win32:CONFIG(release, debug|release): TARGET = TPZ001_CMD
else:win32:CONFIG(debug, debug|release): TARGET = TPZ001_CMDd
else:unix: TARGET = TPZ001_CMD

DESTDIR = ../../bin
TEMPLATE = lib

DEFINES += TPZ001_CMD_LIBRARY

SOURCES += ./src/tpz001_cmd.cpp \
    ./src/apt_convert.cpp \
    ./src/apt_handler.cpp \
    ./src/GenericInstrument.cpp

HEADERS += ./src/tpz001_cmd.h\
    ./src/apt_convert.h \
    ./src/apt_handler.h \
    ./src/apt_type.h \
    ./src/cir_buf.h \
    ./src/GenericInstrument.h \
    ./src/WinDef.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
