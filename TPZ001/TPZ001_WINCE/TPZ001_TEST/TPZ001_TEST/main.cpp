#include "tpz001_test.h"
#include <QtWidgets/QApplication>
#include "tpz001_ui.h"
#include<QLabel>
#include<QMessageBox>
int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

    int handle =  LoadDevice("");
    if(handle<0) {
        QMessageBox msg;
        msg.setText("May be the uart dll is missing!");
        msg.exec();
        return -1;
    }
    QWidget *w =GetFScreen(handle);
	if(w)
	{
    w->show();
	}
    app.exec();
    ExitDevice(handle);

	return app.exec();
}
