/************************************************************************/
// FILE NAME: tpz001_ui.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: Interfaces of ui library
/************************************************************************/
#ifndef TPZ001_UI_GLOBAL_H
#define TPZ001_UI_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QWidget>

#if defined(TPZ001_UI_LIBRARY)
#  define TPZ001_UISHARED_EXPORT Q_DECL_EXPORT
#else
#  define TPZ001_UISHARED_EXPORT Q_DECL_IMPORT
#endif

/// <summary>
/// InitDevice function. Load and Check UART_LIBRARY dll
/// </summary>
/// <param name="sn">set the serial number which you want to open</param>
/// <returns> non-negtive number: the handle number of the opened device; negtive number : failed.</returns>
extern "C" TPZ001_UISHARED_EXPORT int LoadDevice(QString sn);
/// <summary>
/// disconnect with the device and remove the device from device list
/// </summary>
/// <param name="hDevice">the handle number returned from LoadDevice function</param>
/// <returns> non-negtive number: Successfully; negtive number : failed.</returns>
extern "C" TPZ001_UISHARED_EXPORT int ExitDevice(int hDevice);
/// <summary>
/// Get the fullscreen widget's pointer
/// </summary>
/// <param name="hDevice">the handle number returned from LoadDevice function</param>
/// <returns> not 0: the pointer of fullscreen; 0 : failed.</returns>
extern "C" TPZ001_UISHARED_EXPORT QWidget* GetFScreen(int hDevice);
/// <summary>
/// Get the quaterscreen widget's pointer
/// </summary>
/// <param name="hDevice">the handle number returned from LoadDevice function</param>
/// <returns> not 0: the pointer of quaterscreen; 0 : failed.</returns>
extern "C" TPZ001_UISHARED_EXPORT QWidget* GetQScreen(int hDevice);
/// <summary>
/// provide the interfaces of controlling UI for hardware.
/// </summary>
/// <param name="hDevice">the handle number returned from LoadDevice function</param>
/// <param name="cmd">the command name</param>
/// <returns> non-negtive number: Successfully; negtive number : failed.</returns>
extern "C" TPZ001_UISHARED_EXPORT int SendAction(int hDevice,QString cmd);

#endif // TPZ001_UI_GLOBAL_H
