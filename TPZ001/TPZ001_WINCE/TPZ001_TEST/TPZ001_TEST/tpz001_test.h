#ifndef TPZ001_TEST_H
#define TPZ001_TEST_H

#include <QtWidgets/QMainWindow>
#include "ui_tpz001_test.h"

class TPZ001_TEST : public QMainWindow
{
	Q_OBJECT

public:
	TPZ001_TEST(QWidget *parent = 0);
	~TPZ001_TEST();

private:
	Ui::TPZ001_TESTClass ui;
};

#endif // TPZ001_TEST_H
