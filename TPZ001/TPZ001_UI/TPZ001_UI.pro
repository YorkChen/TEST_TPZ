#-------------------------------------------------
#
# Project created by QtCreator 2014-05-07T16:35:09
#
#-------------------------------------------------

QT       += widgets xml core gui

win32:CONFIG(release, debug|release): TARGET = TPZ001_UI
else:win32:CONFIG(debug, debug|release): TARGET = TPZ001_UId
else:unix: TARGET = TPZ001_UI

DESTDIR = ../../bin
TEMPLATE = lib

DEFINES += TPZ001_UI_LIBRARY

SOURCES += \
    ./src/FullScreen.cpp \
    ./src/devicemodel.cpp \
    ./src/ochestrator.cpp \
    ./src/tpz001_ui.cpp \
    ./src/writeworker.cpp \
    ./src/readworker.cpp \
    ./src/syncdeviceworker.cpp \
    ./src/Controller.cpp \
    ./src/steptablemodel.cpp \
    src/pointstep.cpp \
    src/optionsview.cpp \
    src/doubledelegate.cpp \
    src/QuaterScreen.cpp \
    src/output.cpp \
    src/SetCycle.cpp \
    src/InputWidget.cpp \
    src/dragabletableview.cpp \
    src/devicedialog.cpp \
    src/Tools.cpp \
    src/syncworker.cpp \
    src/transfer.cpp

HEADERS +=\
    src/tpz001_ui.h \
    src/devicemodel.h \
    src/ochestrator.h \
    src/writeworker.h \
    src/readworker.h \
    src/syncdeviceworker.h \
    src/apt_type.h \
    src/writenode.h \
    src/Controller.h \
    src/steptablemodel.h \
    src/pointstep.h \
    src/config.h \
    src/optionsview.h \
    src/doubledelegate.h \
    src/QuaterScreen.h \
    src/output.h \
    src/SetCycle.h \
    src/FullScreen.h \
    src/InputWidget.h \
    src/dragabletableview.h \
    src/devicedialog.h \
    src/Tools.h \
    src/syncworker.h \
    src/transfer.h \
    ../TPZ001_CMD/src/tpz001_cmd.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

FORMS += \
    ./src/FullScreen.ui \
    src/ListPorts.ui \
    src/optionsview.ui \
    src/QuaterScreen.ui \
    src/output.ui \
    src/SetCycle.ui \
    src/InputWidget.ui \
    src/devicedialog.ui

RESOURCES += \    
    tpz001.qrc

win32:CONFIG(release, debug|release): LIBS +=-lThorlabsCustomWidgets -L$$PWD/../../bin/ -lTPZ001_CMD
else:win32:CONFIG(debug, debug|release): LIBS +=-lThorlabsCustomWidgetsd -L$$PWD/../../bin/ -lTPZ001_CMDd
else:unix: LIBS += -L$$PWD/../../bin/ -lTPZ001_CMD

#win32:CONFIG(release, debug|release): DEFINES += QT_NO_DEBUG_OUTPUT
#else:win32:CONFIG(debug, debug|release):

INCLUDEPATH += $$PWD/../../bin
DEPENDPATH += $$PWD/../../bin
