#include<QFileDialog>
#include<QtGui>
#include<QDomDocument>
#include "FullScreen.h"
#include "SetCycle.h"
#include"ui_FullScreen.h"
#include "ui_ListPorts.h"
#include "optionsview.h"
#include"InputWidget.h"
#include"output.h"
#include<QMessageBox>
#include<ThorlabsCustomWidgets/efiledialog.h>
FullScreen::FullScreen(QWidget *parent,bool showDevicelist) :
    QMainWindow(parent),
    ui(new Ui::FullScreen)
{
    ui->setupUi(this);
    ///set graph widget
    int graphNumber=ui->graph_widget->addGraph();
    m_graphNumberList.append(graphNumber);
    ui->graph_widget->setLegendVisible(true);
    ui->graph_widget->setLabel(0,tr("T(s)"));
    ui->graph_widget->setPen(m_graphNumberList.at(0),QPen(Qt::darkCyan));
    ui->graph_widget->setName(m_graphNumberList.at(0),"V(v)");
    ui->graph_widget->setXAxisFormat(TPlot::DateTime);
    ui->action_Email->setEnabled(true);

    AddLogo();

    m_isDragging = false;

    m_model = NULL;
    m_controller = NULL;

    ui->sliderVolt->setMaximum(75);
    ui->spinVolt->setSingleStep(1);

    m_showDeviceList = showDevicelist;
    m_devDia=new DeviceDialog(this);
    m_devDia->hide();

    m_deviceUI = ui->stackedWidget->currentWidget();
    m_options =new OptionsView(this);
    ui->stackedWidget->addWidget(m_options);
}

void FullScreen::AddLogo()
{
    QIcon logoIcon;
    logoIcon.addFile(QString::fromUtf8(":/Resources/Thorlabs_Logo_Red.png"), QSize(), QIcon::Normal, QIcon::Off);

    QToolButton *toolButton_T_logo = new QToolButton(this);
    toolButton_T_logo->setObjectName(QString::fromUtf8("toolButton_T_logo"));
    toolButton_T_logo->setStyleSheet("QToolButton{border:0px;}");
    toolButton_T_logo->setIcon(logoIcon);
    toolButton_T_logo->setIconSize(QSize(100, 16));

    connect(toolButton_T_logo,SIGNAL(clicked()),this,SLOT(m_slot_JumpToWebsite()));

    QWidget *outSideWidget = new QWidget(this);
    QVBoxLayout *vboxLayout =new QVBoxLayout(outSideWidget);
    vboxLayout->setContentsMargins(0,0,0,0);
    vboxLayout->addWidget(toolButton_T_logo);
    vboxLayout->addStretch();
    ui->logoToolbar->addWidget(outSideWidget);
}
void FullScreen::m_slot_JumpToWebsite()
{
    QDesktopServices::openUrl(QUrl("http://www.thorlabs.com"));
}
void FullScreen::closeEvent(QCloseEvent *)
{
    m_controller->DisConnectDevice();
}

FullScreen::~FullScreen()
{
    delete ui;
}


void FullScreen::Init(Controller *ctrl,DeviceModel* model)
{
    m_controller = ctrl;
    m_model = model;
    InitConnections();
    BindDataSource();
    ui->input_widget->init(ctrl,model);
    ui->output_widget->Init(ctrl,model);
}
void FullScreen::BindDataSource()
{
    m_binding.insert("OUTPUTVOLT",&FullScreen::UpdateVolt);
    m_binding.insert("DEVICEISCONNECT",&FullScreen::UpdateConnection);
    m_binding.insert("MAXVOLT",&FullScreen::UpdateMaxVolt);
    m_binding.insert("ISSTEPRUNNING",&FullScreen::UpdateIsStepRunning);
}

void FullScreen::InitConnections()
{
    connect(m_model,SIGNAL(PropertyChanged(QString)),
            this,SLOT(ModelPropertyChanged(QString)),Qt::QueuedConnection);
    connect(ui->action_Connect,SIGNAL(triggered()),
            this,SLOT(btnConnect_Clicked()));
    connect(ui->action_Disconnect,SIGNAL(triggered()),
            m_controller,SLOT(DisConnectDevice()));
//    connect(ui->action_Identify,SIGNAL(triggered()),
//            m_controller,SLOT(Identify()));
    connect(ui->Identify,SIGNAL(clicked()),m_controller,SLOT(Identify()));
    connect(m_options,SIGNAL(finished(int)),this,SLOT(onOptionclosed()));
}

void FullScreen::ModelPropertyChanged(QString propName)
{
    QString name = propName.toUpper();
    if(m_binding.contains(name))
    {
        fooPtr  fp = m_binding.value(name);
        (this->*fp)();
    }
}
void FullScreen::Resize(int w, int h, int type)
{

    QWidget* wid=NULL;
    switch (type) {
    case 0:
        wid=MainView();
        break;
    case 1:
        wid =TopView();
        break;
    case 2:
        wid=StatusView();
        break;
    default:
        break;
    }
    wid->resize(w,h);
    wid->move(0,0);
}



void FullScreen::UpdateVolt()
{
    static qint64 lastTime=0;
    if(lastTime!=QDateTime::currentMSecsSinceEpoch()/1000)
    {
    ui->graph_widget->addData(m_graphNumberList.at(0),QDateTime::currentMSecsSinceEpoch()/1000,m_model->GetOutputVolt());
    lastTime=QDateTime::currentMSecsSinceEpoch()/1000;
    }
    if(!ui->graph_widget->isPausing())
    {
        ui->graph_widget->rescaleKeyAxis(m_graphNumberList.at(0));
        ui->graph_widget->replot();
    }
}
void FullScreen::UpdateConnection()
{
    if(m_model->GetDeviceIsConnect())
    {
        ui->action_Connect->setVisible(false);
        ui->action_Disconnect->setVisible(true);
        ui->centralWidget->setEnabled(true);
        ui->tabWidget_main->setEnabled(true);//if used in platform
        ui->action_Option->setEnabled(true);
        ui->action_Save->setEnabled(true);
        ui->action_Load->setEnabled(true);

        ui->output_widget->ui->lblSn->setText("TPZ001 sn: "+ m_controller->GetSN());
        setWindowTitle("TPZ "+ m_controller->GetSN());

    }
    else
    {
        ui->action_Connect->setVisible(true);
        ui->action_Disconnect->setVisible(false);
        ui->centralWidget->setEnabled(false);
        ui->tabWidget_main->setEnabled(false);//if used in platform
        ui->action_Option->setEnabled(false);
        ui->action_Save->setEnabled(false);
        ui->action_Load->setEnabled(false);
    }
}

void FullScreen::UpdateMaxVolt()
{
    ui->spinVolt->setMaximum(m_model->GetMaxVolt());
    ui->sliderVolt->setMaximum(m_model->GetMaxVolt());
}


void FullScreen::on_sliderVolt_valueChanged(int value)
{  
    ui->spinVolt->setValue(value);
    if(!m_isDragging)
    {
        m_controller->SetVolt(value);
    }
}

void FullScreen::on_sliderVolt_sliderReleased()
{
    m_controller->SetVolt(ui->spinVolt->value());
    m_isDragging =false;
}

void FullScreen::on_sliderVolt_sliderPressed()
{
    m_isDragging = true;
}
void FullScreen::RefreshDeviceList()
{
    QList<QString> list;
    m_controller->GetDeviceList(list);
    m_devDia->RefreshList(list);
}

void FullScreen::btnConnect_Clicked()
{
    //clear graph
    for(int r = 0; r < m_graphNumberList.size(); r++) {
        ui->graph_widget->clearData(m_graphNumberList[r]);
    }
    if(m_showDeviceList)
    {
        QString sn;
        RefreshDeviceList();
        if(m_devDia->exec())//accept
        {
            sn=  m_devDia->GetSN();
        }
        if(sn!=NULL&&!sn.isEmpty())
        {
            m_controller->ConnectDevice(sn);
        }
    }
    else
        m_controller->ConnectDevice();
}
void FullScreen::UpdateIsStepRunning()
{
    bool is_running = m_model->GetIsStepRunning();

    if(is_running) {
        ui->control_widget->setEnabled(false);
        ui->action_Load->setEnabled(false);
    }
    else {
        if(m_model->GetDeviceIsConnect())
        {
            ui->control_widget->setEnabled(true);
            ui->action_Load->setEnabled(true);
        }
    }
}

void FullScreen::on_action_Option_triggered()
{
    if(m_options!=NULL)
    {
        m_options->Init(m_controller,m_model);
        ui->stackedWidget->setCurrentWidget(m_options);
    }
}

void FullScreen::onOptionclosed()
{
    ui->stackedWidget->setCurrentWidget(m_deviceUI);
}

void FullScreen::on_btnJogLeft_clicked()
{
    m_controller->SetVolt(m_model->GetOutputVolt() - m_model->GetJogStepSize());
}

void FullScreen::on_btnJogRight_clicked()
{
    m_controller->SetVolt(m_model->GetOutputVolt() + m_model->GetJogStepSize());
}

void FullScreen::on_btnReset_clicked()
{
    m_controller->SetVolt(0);
}


void FullScreen::on_action_Save_triggered()
{
    QString fileName;
#ifndef Q_OS_WINCE
    fileName=EFileDialog::getSaveFileName(this,"Save",QDir::currentPath(),"config(*.xml)");
#else
    fileName=EFileDialog::getSaveFileName(this,"Save","\\THORLABS\\TPZ001\\","config(*.xml)");
#endif
    SaveConfig(fileName);
}

void FullScreen::on_action_Load_triggered()
{
    QString fileName;
#ifndef Q_OS_WINCE
    fileName=EFileDialog::getOpenFileName(this,"Load",QDir::currentPath(),"config(*.xml)");
#else
    fileName=EFileDialog::getOpenFileName(this,"Load","\\THORLABS\\TPZ001\\","config(*.xml)");
#endif
    LoadConfig(fileName);
}
void FullScreen::on_spinVolt_editingFinished()
{
    double value = ui->spinVolt->value();
    ui->sliderVolt->setValue(value);
    m_controller->SetVolt(value);
}
void FullScreen::SaveConfig(QString fileName)
{

    if(!fileName.isNull())
    {
        QFile file(fileName);
        if(file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QXmlStreamWriter writer(&file);
            writer.setAutoFormatting(true);

            writer.writeStartDocument();
            writer.writeStartElement("Config");
            writer.writeTextElement("MaxVolt", QString::number(m_model->GetMaxVolt()));
            writer.writeTextElement("ControleMode",QString::number(m_model->GetControlMode()));
            writer.writeTextElement("HubAnalogInput",QString::number(m_model->GetHubAnalogInput()));
            writer.writeTextElement("InputSrcMode",QString::number(m_model->GetInputSourceMode()));
            writer.writeTextElement("LoopProp", QString::number(m_model->GetLoopProp()));
            writer.writeTextElement("LoopIntegral", QString::number(m_model->GetLoopIntegral()));
            writer.writeTextElement("DisplayIntensity", QString::number(m_model->GetIntensity()));
            writer.writeTextElement("JogStepSize", QString::number(m_model->GetJogStepSize()));

            writer.writeStartElement("Steps");

            for(int i=0;i<ui->input_widget->m_listSteps.length();i++)
            {
                writer.writeStartElement("Step");
                writer.writeTextElement("Volt", QString::number(ui->input_widget->m_listSteps[i]->GetVolt()));
                writer.writeTextElement("Duration", QString::number(ui->input_widget->m_listSteps[i]->GetDuration()));
                writer.writeEndElement();
            }
            writer.writeEndElement();

            writer.writeEndElement();
            writer.writeEndDocument();
            file.close();
        }
    }

}

void FullScreen::LoadConfig(QString fileName)
{
    QFile file(fileName);
    int loopProp =m_model->GetLoopProp();
    int loopIntegral = m_model->GetLoopIntegral();
    double maxVolt = m_model->GetMaxVolt();
    HUB_ANALOGINPUT hubInput =m_model->GetHubAnalogInput();
    bool convertSucceed = false;

    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QXmlStreamReader reader(&file);
        QDomDocument doc;
        if(!doc.setContent(&file))
        {
            file.close();
        }
        QDomElement root = doc.documentElement();
        QDomNode n = root.firstChild();
        QDomNodeList elements = root.elementsByTagName("MaxVolt");
        if(elements.count()>0)
        {
            QString  str = elements.at(0).toElement().text();
            double value =str.toDouble(&convertSucceed);
            if(convertSucceed) maxVolt = value;
        }

        elements = root.elementsByTagName("HubAnalogInput");
        if(elements.count()>0)
        {
            QString  str = elements.at(0).toElement().text();
            int value =str.toDouble(&convertSucceed);
            if(convertSucceed)
                hubInput = (HUB_ANALOGINPUT)value;
        }

        elements = root.elementsByTagName("InputSrcMode");
        if(elements.count()>0)
        {
            QString  str = elements.at(0).toElement().text();
            int value  = str.toDouble(&convertSucceed);
            if(convertSucceed)
                m_controller->SetInputSrcMode((INPUTSRC_MODE)value);
        }

        elements = root.elementsByTagName("LoopProp");
        if(elements.count()>0)
        {
            QString  str = elements.at(0).toElement().text();
            int value =str.toDouble(&convertSucceed);
            if(convertSucceed)
                loopProp = value;
        }

        elements = root.elementsByTagName("LoopIntegral");
        if(elements.count()>0)
        {
            QString  str = elements.at(0).toElement().text();
            int value =str.toDouble(&convertSucceed);
            if(convertSucceed)
                loopIntegral =value;
        }

        elements = root.elementsByTagName("DisplayIntensity");
        if(elements.count()>0)
        {
            QString  str = elements.at(0).toElement().text();
            int value =str.toDouble(&convertSucceed);
            if(convertSucceed)
                m_controller->SetIntensity(value);
        }

        elements = root.elementsByTagName("JogStepSize");
        if(elements.count()>0)
        {
            QString  str = elements.at(0).toElement().text();
            double value =str.toDouble(&convertSucceed);
            if(convertSucceed)
            {
                //                m_controller->m_jogStepSize = value;
                m_model->SetJogStepSize(value);
            }
        }

        elements = root.elementsByTagName("Steps");
        if(elements.count()>0)
        {
            ui->input_widget->m_stepTableModel->Clear();
            QDomNodeList stepNodes = elements.at(0).toElement().elementsByTagName("Step");
            QList<PointStep*> listSteps;
            int id=0;
            for(int i=0;i<stepNodes.count();i++)
            {
                QDomElement eleStep = stepNodes.at(i).toElement();
                PointStep* step = new PointStep();
                step->SetId(++id);
                elements = eleStep.elementsByTagName("Volt");
                if(elements.count()>0)
                {
                    QString  str = elements.at(0).toElement().text();
                    double value =str.toDouble(&convertSucceed);
                    if(convertSucceed)
                        step->SetVolt(value);
                    else
                    {
                        delete step;
                        break;
                    }
                }

                elements = eleStep.elementsByTagName("Duration");
                if(elements.count()>0)
                {
                    QString  str = elements.at(0).toElement().text();
                    double value =str.toDouble(&convertSucceed);
                    if(convertSucceed)
                        step->SetDuration(value);
                    else
                    {
                        delete step;
                        break;
                    }
                }
                listSteps.append(step);
            }
            if(listSteps.count()>0)
            {
                ui->input_widget->m_stepTableModel->AddSteps(listSteps);
            }

        }
        m_controller->SetIOSettings(maxVolt,hubInput);
        m_controller->SetLoopConsts(loopProp,loopIntegral);
    }
    file.close();

}


void FullScreen::on_action_Email_triggered()
{
    if(!QDesktopServices::openUrl(QUrl("mailto:techsupport@thorlabs.com?subject=Software\ Help&body=Dear:\n This is a help mail. \n")))
    {
        QMessageBox msg;
        msg.setText("No email client installed, please contact with techsupport@thorlabs.com");
        msg.exec();
    }
}
QWidget* FullScreen::TopView()
{
    return ui->mainToolBar;
}
QWidget* FullScreen::StatusView()
{
    return ui->output_widget;
}
QWidget* FullScreen::MainView()
{
    return ui->tab_home;
}
