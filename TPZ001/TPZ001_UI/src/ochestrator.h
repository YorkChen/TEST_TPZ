/************************************************************************/
// FILE NAME: ochestrator.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: Contains fullscreen quaterscreen model and controller
/************************************************************************/
#ifndef OCHESTRATOR_H
#define OCHESTRATOR_H

#include<QtCore>
#include<QtGui>
#include <QWidget>
#include "FullScreen.h"
#include "QuaterScreen.h"
#include"Controller.h"
class Ochestrator
{
public:    
    Ochestrator(QString sn, int nbaud, int timeout);
    ~Ochestrator();

    QWidget* GetFullScrren();

    QWidget* GetQuaterScreen();

    Controller* GetController(){return m_controller;}

    DeviceModel* GetModel(){return m_model;}
    void Exit();


    int SendAction(QString cmd);
    void JogUp();

    void JogDown();

    void ResetToZero();
    void WheelNegative();
    void WheelPositive();

private:
    FullScreen *fullScreen;
    QuaterScreen * quaterScreen;
    Controller * m_controller;
    QString m_sn;
    DeviceModel* m_model;

};

#endif // OCHESTRATOR_H
