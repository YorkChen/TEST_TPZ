/************************************************************************/
// FILE NAME: readworker.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: Read thread,read data from device
/************************************************************************/
#ifndef READWORKER_H
#define READWORKER_H

#include <QObject>
#include<QThread>
#include<QWaitCondition>
#include<QMutex>
#include<QHash>
#include "apt_type.h"
#include "devicemodel.h"


class ReadWorker : public QThread
{
    Q_OBJECT
public:
    explicit ReadWorker(QObject *parent = 0);
    void Init(int handle, DeviceModel * model);

signals:
    void finished();
    void deviceDisconnected();
public slots:
    void DoWork();
    void Stop(){
        m_stopped = true;
        emit finished();

        quit();
        wait();
    }
protected:
    void run();

private:
    void Proc();
    void InitFuncMap();
    void ReadOutPutVolt(char* buf);
    void ReadControlMode(char* buf);
    void ReadIOSettings(char* buf);
    void ReadInputSrc(char* buf);
    void ReadPIConsts(char *oBuf);
    void ReadDISPSettings(char *oBuf);
    typedef void (ReadWorker::*ReadPtr)(char* buf);
    QHash<APT_RESPONSE_MESSAGE_TYPE,ReadPtr> m_readFuncMap;
    int m_handle;
    bool m_stopped;
    bool m_quit;
    RESPONSE_MSG msg;
    DeviceModel *m_model;
};

#endif // READWORKER_H
