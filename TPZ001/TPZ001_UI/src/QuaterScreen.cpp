#include "QuaterScreen.h"
#include"ui_QuaterScreen.h"
#include "ui_output.h"
#include <QDebug>
#include"output.h"
QuaterScreen::QuaterScreen(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QuaterScreen)
{
    ui->setupUi(this);
    m_connectIcon =QIcon(":/Resources/connect.png");
    m_disconnectIcon =QIcon(":/Resources/disconnect.png");

    m_maxVolt =75;
    m_minVolt =0;
    m_spinStep =0.1;
    m_IsModelUpdating = false;
    m_IsModifyVolt =false;

    m_model = NULL;
    m_controller = NULL;
}

QuaterScreen::~QuaterScreen()
{
    delete ui;
}
void QuaterScreen::closeEvent(QCloseEvent *)
{
    m_controller->DisConnectDevice();
}

void QuaterScreen::Init(Controller *ctrl,DeviceModel* model)
{
    m_controller = ctrl;
    m_model = model;
    BindDataSource();
    InitConnections();
    ui->output_widget->Init(ctrl,model);
}

void QuaterScreen::ModelPropertyChanged(QString propName)
{
    m_IsModelUpdating = true;

    QString name = propName.toUpper();
    if(m_binding.contains(name))
    {
        fooPtr  fp = m_binding.value(name);
        (this->*fp)();
    }

    m_IsModelUpdating = false;
}

void QuaterScreen::BindDataSource()
{
    m_binding.insert("DEVICEISCONNECT",&QuaterScreen::UpdateConnection);
}


void QuaterScreen::InitConnections()
{
    connect(m_model,SIGNAL(PropertyChanged(QString)),
            this,SLOT(ModelPropertyChanged(QString)),Qt::QueuedConnection);
}
void QuaterScreen::UpdateConnection()
{
    if(m_model->GetDeviceIsConnect())
    {
        ui->btnConnect->setIcon(m_disconnectIcon);
        ui->btnJogLeft->setEnabled(true);
        ui->btnJogRight->setEnabled(true);
        ui->btnReset->setEnabled(true);
        ui->output_widget->ui->lblSn->setText("TPZ001 sn: "+ m_controller->GetSN());
    }
    else
    {
        ui->btnConnect->setIcon(m_connectIcon);
        ui->btnJogLeft->setEnabled(false);
        ui->btnJogRight->setEnabled(false);
        ui->btnReset->setEnabled(false);
    }
}
void QuaterScreen::on_btnConnect_clicked()
{
    if(m_model->GetDeviceIsConnect())
    {
        m_controller->DisConnectDevice();
    }
    else
    {
        m_controller->ConnectDevice();
    }
}

void QuaterScreen::on_btnJogLeft_clicked()
{
    m_controller->SetVolt(m_model->GetOutputVolt() -m_model->GetJogStepSize());
}

void QuaterScreen::on_btnReset_clicked()
{
    m_controller->SetVolt(0);
}

void QuaterScreen::on_btnJogRight_clicked()
{
    m_controller->SetVolt(m_model->GetOutputVolt() +m_model->GetJogStepSize());
}
