/************************************************************************/
// FILE NAME: FullScreen.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: Full screen ,contains common setting toolbar ,output widget,customplot,steplistview,
// and so on.
/************************************************************************/
#ifndef FullScreen_H
#define FullScreen_H

#include <QWidget>
#include<QHash>
#include<QMainWindow>
#include "Controller.h"
#include "ui_ListPorts.h"
#include<QIcon>
#include "optionsview.h"
#include "doubledelegate.h"
#include"devicedialog.h"
namespace Ui {
class FullScreen;
}

class FullScreen : public QMainWindow
{
    Q_OBJECT

public:
    explicit FullScreen(QWidget *parent = 0,bool showDevicelist =true);
    ~FullScreen();
    void Init(Controller *ctrl,DeviceModel* model);
    void SaveConfig(QString fileName);
    void LoadConfig(QString fileName);
    QWidget* TopView();
    QWidget* StatusView();
    QWidget* MainView();
    bool ConnectDevice(QString sn){ return m_controller->ConnectDevice(sn); }
    void DisConnectDevice(){m_controller->DisConnectDevice();}
    bool IsConnected(){return m_model->GetDeviceIsConnect();}
protected:
    void closeEvent(QCloseEvent *);
public slots:
    void ModelPropertyChanged(QString propName);
    void Resize(int w,int h,int type);
//private slots:
    void on_sliderVolt_valueChanged(int value);

    void on_sliderVolt_sliderReleased();

    void on_sliderVolt_sliderPressed();

    void RefreshDeviceList();

    void btnConnect_Clicked();

    void on_action_Option_triggered();

    void onOptionclosed();

    void on_btnJogLeft_clicked();

    void on_btnJogRight_clicked();

    void on_btnReset_clicked();

    void on_action_Save_triggered();

    void on_action_Load_triggered();

    void on_spinVolt_editingFinished();

    void m_slot_JumpToWebsite();


    void on_action_Email_triggered();


private:
    typedef void (FullScreen::*fooPtr)();
    void AddLogo();
    void BindDataSource();
    void InitConnections();
    void UpdateVolt();
    void UpdateConnection();
    void UpdateMaxVolt();
    void UpdateIsStepRunning();
    Ui::FullScreen *ui;
    Controller *m_controller;
    QHash<QString,fooPtr> m_binding;
    DeviceModel *m_model;
//    int m_timeCnt;
//    double m_minVolt;
    bool m_isDragging;
    bool m_showDeviceList;
    QWidget* m_deviceUI;
    OptionsView * m_options;
    QList<int> m_graphNumberList;

    DeviceDialog * m_devDia;
};

#endif // FullScreen_H
