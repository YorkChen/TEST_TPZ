/************************************************************************/
// FILE NAME: devicemodel.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: Defines model which stores data.
/************************************************************************/
#ifndef DEVICEMODEL_H
#define DEVICEMODEL_H

#define TPROPERTY(type,name) \
    type m_##name;\
    type Get##name (){return m_##name;};\
    void Set##name(type value){m_##name=value; emit PropertyChanged(#name);};

#include<QString>
#include<QObject>
#include "apt_type.h"
class DeviceModel : public QObject
{
    Q_OBJECT
public:
    DeviceModel();


    TPROPERTY(double,OutputVolt)
    TPROPERTY(bool,DeviceIsConnect)
    TPROPERTY(double,MaxVolt)
    TPROPERTY(CONTROL_MODE,ControlMode)
    TPROPERTY(HUB_ANALOGINPUT,HubAnalogInput)
    TPROPERTY(INPUTSRC_MODE,InputSourceMode)
    TPROPERTY(int,LoopProp)
    TPROPERTY(int,LoopIntegral)
    TPROPERTY(int,Intensity)
    TPROPERTY(double,JogStepSize)
    TPROPERTY(bool,IsStepRunning)

    void Clear();
    static QString dllPath;
signals:
    void PropertyChanged(const QString& propName);
private:

};

#endif // DEVICEMODEL_H
