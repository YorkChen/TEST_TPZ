#include "dragabletableview.h"
#include<QDragMoveEvent>
#include<QMouseEvent>
#include<QDrag>
#include<QMimeData>
#include<QGuiApplication>
#include<QScreen>
#include<QHeaderView>
#include<QStandardItem>
#include<QAbstractItemModel>
#include<QDebug>
#include"steptablemodel.h"
DragableTableView::DragableTableView(QWidget *parent) :
    QTableView(parent)
{
    connect(verticalHeader(),SIGNAL(sectionMoved(int,int,int)),this,SLOT(ResetLogicalindex(int,int,int)));
}
void DragableTableView::ResetLogicalindex(int logicalindex, int oldindex, int newindex)
{
    qDebug()<<"logical index ="<<logicalindex<<"old index = "<<oldindex<<"new index"<<newindex;
    StepTableModel* stepmodel=qobject_cast<StepTableModel*> (model());
    PointStep* step= stepmodel->takeStep(logicalindex,newindex);
    QList<int> rows;
    rows.append(logicalindex);
    stepmodel->Remove(rows);
    stepmodel->InsertStep(step,newindex-1);
}
