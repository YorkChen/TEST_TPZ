/************************************************************************/
// FILE NAME: pointstep.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: Defines the struct of step, used in step listView.
/************************************************************************/
#ifndef POINTSTEP_H
#define POINTSTEP_H

class PointStep
{
public:
    PointStep();

    int GetId(){return m_id;}
    void SetId(int id){ m_id =id;}
    void SetVolt(double p){ m_voltage =p; }
    double GetVolt(){ return m_voltage; }
    void SetDuration(double d) { m_duration = d; }
    double GetDuration(){ return m_duration; }

private:
    int m_id;
    double m_voltage;
    double m_duration;
};

#endif // POINTSTEP_H
