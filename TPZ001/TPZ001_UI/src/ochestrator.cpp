#include "ochestrator.h"

Ochestrator::Ochestrator(QString sn, int nbaud, int timeout):m_model(NULL)
{
    m_model = new DeviceModel();
    m_controller = new Controller(sn,nbaud,timeout,m_model);
    fullScreen = NULL;
    quaterScreen = NULL;
    m_sn = sn;
}

Ochestrator::~Ochestrator()
{
    if(fullScreen!=NULL)
    {
        delete fullScreen;
        fullScreen=NULL;
    }
    if(quaterScreen != NULL)
    {
        delete quaterScreen;
        quaterScreen=NULL;
    }
    if(m_controller!=NULL)
    {
        delete m_controller;
        m_controller=NULL;
    }
    if(m_model!=NULL)
    {
        delete m_model;
        m_model=NULL;
    }
}
QWidget* Ochestrator::GetFullScrren()
{
    if(fullScreen==NULL)
    {

        fullScreen = new FullScreen(NULL,m_sn.isEmpty());
        fullScreen->Init(m_controller,m_model);
    }
    return (QWidget*)fullScreen;
}
QWidget* Ochestrator::GetQuaterScreen()
{
    if(quaterScreen==NULL)
    {
        quaterScreen = new QuaterScreen();
        quaterScreen->Init(m_controller,m_model);
    }
    return (QWidget*)quaterScreen;
}
void Ochestrator::Exit()
{
    m_controller->DisConnectDevice();
}
int Ochestrator::SendAction(QString cmd)
{

    if(cmd==  "JogUp") {
        JogUp();
       }
    else if(cmd == "JogDown")
    {
         JogDown();
    }
    else if (cmd == "Reset")
    {
        ResetToZero();
    }
    else if(cmd == "WheelPositive")
    {
        WheelPositive();
    }
    else if(cmd == "WheelNegative")
    {
        WheelNegative();
    }
    return 0;
}


void Ochestrator::JogUp()
{
    if(!m_model->GetDeviceIsConnect()|| m_model->GetIsStepRunning())
    {
        return;
    }
    m_controller->SetVolt(m_model->GetOutputVolt() + m_model->GetJogStepSize());
}
void Ochestrator::JogDown()
{
    if(!m_model->GetDeviceIsConnect()|| m_model->GetIsStepRunning())
    {
        return;
    }
    m_controller->SetVolt(m_model->GetOutputVolt() - m_model->GetJogStepSize());
}
void Ochestrator::ResetToZero()
{
    if(!m_model->GetDeviceIsConnect()|| m_model->GetIsStepRunning())
    {
        return;
    }
    m_controller->SetVolt(0);
}
void Ochestrator::WheelNegative()
{
    double currentVolt=m_model->GetOutputVolt();
    double value=currentVolt-0.002;
    if(value<0)
    {
        value=0;
    }
    m_controller->SetVolt(value);
}
void Ochestrator::WheelPositive()
{
    double currentVolt=m_model->GetOutputVolt();
    double value=currentVolt+0.002;
    if(value>m_model->GetMaxVolt())
    {
        value=m_model->GetMaxVolt();
    }
    m_controller->SetVolt(value);
}
