#ifndef DRAGABLETABLEVIEW_H
#define DRAGABLETABLEVIEW_H

#include <QTableView>

class DragableTableView : public QTableView
{
    Q_OBJECT
public:
    explicit DragableTableView(QWidget *parent = 0);

signals:

public slots:
    void ResetLogicalindex(int ,int ,int);
};

#endif // DRAGABLETABLEVIEW_H
