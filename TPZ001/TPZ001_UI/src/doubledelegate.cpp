#include "doubledelegate.h"
#include<QDebug>
#include<QDebug>
DoubleDelegate::DoubleDelegate(QObject *parent)
    :QItemDelegate(parent)
{
    m_min=0;
    m_max =100;
    m_trimLength = 0;
}


QWidget* DoubleDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex &index) const
{
    emit const_cast<DoubleDelegate*>(this)->EditorCreated(index);

    QDoubleSpinBox *editor = new QDoubleSpinBox(parent);

    double value = index.model()->data(index, Qt::DisplayRole).toDouble();
    const_cast<DoubleDelegate*>(this)->SetValue(value);
    if(index.column()==2)
        editor->setDecimals(1);
    return editor;
}

void DoubleDelegate::SetRange(double min,double max)
{
    m_min =min;
    m_max = max;
}

void DoubleDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox*>(editor);
    spinBox->setRange(m_min,m_max);
    QString str = index.model()->data(index, Qt::DisplayRole).toString();
    if(m_trimLength != 0)
        str =  str.mid(0,str.length()-m_trimLength);
    double value =str.toDouble();
    spinBox->setValue(value);
}

void DoubleDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox*>(editor);
    model->setData(index, spinBox->value(), Qt::EditRole);
    if(m_preValue != spinBox->value())
    {
        emit const_cast<DoubleDelegate*>(this)->valuechanged(spinBox->value(),index);

    }
}

void DoubleDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}
