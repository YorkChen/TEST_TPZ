#include "Controller.h"
#include "../../TPZ001_CMD/src/tpz001_cmd.h"
#include<QDebug>
#include<QStringList>
#include "config.h"
#include<qxmlstream.h>
#include<QDomDocument>
#include<QFile>
#include<QMessageBox>

Controller::Controller(QString sn, int nbaud, int timeout,DeviceModel* model)
{
    m_sn = sn;
    m_model = model;
    m_speed = nbaud;
    m_timeout = timeout;
    m_handle =-1;
    connect(&m_readWorker,SIGNAL(deviceDisconnected()),this,SLOT(DisConnectDevice()));
    qRegisterMetaType<DEVICE_MESSAGE>("DEVICE_MESSAGE");
    connect(&m_writeWorker,SIGNAL(emit_error(DEVICE_MESSAGE)),this,SLOT(receive_messages(DEVICE_MESSAGE)));
}

Controller::~Controller()
{

}
void Controller::SetVolt(double v)
{
    write_node_t node;
    node.tx_device = m_handle;
    node.tx_msg_type = APT_MGMSG_PZ_SET_OUTPUTVOLTS;
    node.tx_volt = v;
    m_writeWorker.SendCmd(node);
}


void Controller::GetDeviceList(QList<QString> &list)
{
    char sns[1024]={0};
    int r = fnTPZ001_DLL_GetDeviceList(&sns[0],1024);
    if(r<1) return;
    QString str(sns);
    QStringList snsList = str.split(',');
    for(int i=0;i<snsList.count();i+=2)
    {
        if(snsList.at(i+1).contains("TPZ001"))
        {
            list.append(snsList.at(i));
        }
    }
}

bool Controller::ConnectDevice()
{
    return ConnectDevice(m_sn);
}

bool Controller::ConnectDevice(QString sn)
{
    if(m_model->GetDeviceIsConnect())return true;
    int handle =  fnTPZ001_DLL_OpenDevice(sn.toLatin1().data(),m_speed,m_timeout);
    if(handle > -1)
    {
        m_sn =sn;
        m_handle = handle;
        m_model->SetDeviceIsConnect(true);
        m_model->SetJogStepSize(m_model->GetJogStepSize());
        StartProcDeviceMsg();
        return true;
    }
    return false;
}

void Controller::DisConnectDevice()
{
    if(!m_model->GetDeviceIsConnect()) return;

    StopProcDeviceMsg();

    int ret =  fnTPZ001_DLL_CloseDevice(m_handle);
    if(ret==0)
    {
        m_model->Clear();
        m_model->SetDeviceIsConnect(false);
        m_handle = -1;
    }
    else
        StartProcDeviceMsg();
}

void Controller::StartProcDeviceMsg()
{
    if(m_writeWorker.isRunning()) return;

    m_readWorker.Init(m_handle,m_model);
    m_writeWorker.Init(m_handle);
    m_syncThread.Init(m_handle,&m_writeWorker);

    m_readWorker.start();
    m_writeWorker.start();
    m_syncThread.start();
}

void Controller::StopProcDeviceMsg()
{
    m_syncThread.Stop();
    m_writeWorker.Stop();
    m_readWorker.Stop();

}

void Controller::Identify()
{
    if(!m_model->GetDeviceIsConnect())
    {
        return;
    }

    write_node_t node;
    node.tx_device = m_handle;
    node.tx_msg_type =APT_MGMSG_MOD_IDENTIFY;
    m_writeWorker.SendCmd(node);
}

void Controller::SetVoltLimit(double maxVolt)
{
    write_node_t node;
    node.tx_device =m_handle;
    node.tx_volt =maxVolt;
    node.tx_msg_type =APT_MGMSG_PZ_SET_IOSETTINGS;
    node.tx_volt =maxVolt;
    node.tx_shortData =m_model->GetHubAnalogInput();
    m_writeWorker.SendCmd(node);
}

void Controller::SetControlMode(CONTROL_MODE mode)
{
    write_node_t node;
    node.tx_device =m_handle;
    node.tx_msg_type =APT_MGMSG_PZ_SET_POSCONTROLMODE;
    node.tx_shortData =mode;
    m_writeWorker.SendCmd(node);
}

void Controller::SetIOSettings(double maxVolt,HUB_ANALOGINPUT hubInput)
{
    write_node_t node;
    node.tx_msg_type =APT_MGMSG_PZ_SET_IOSETTINGS;
    node.tx_device =m_handle;
    node.tx_volt =maxVolt;
    node.tx_shortData =hubInput;
    m_writeWorker.SendCmd(node);
}

void Controller::SetInputSrcMode(INPUTSRC_MODE mode)
{
    write_node_t node;
    node.tx_device =m_handle;
    node.tx_msg_type= APT_MGMSG_PZ_SET_INPUTVOLTSSRC;
    node.tx_shortData =mode;
    m_writeWorker.SendCmd(node);
}

void Controller::SetIntensity(int intensity)
{
    write_node_t node;
    node.tx_device =m_handle;
    node.tx_msg_type= APT_MGMSG_PZ_SET_TPZ_DISPSETTINGS;
    node.tx_shortData =(short)intensity;
    m_writeWorker.SendCmd(node);
}

void Controller:: SetLoopConsts(int p,int integral)
{
    write_node_t node;
    node.tx_device =m_handle;
    node.tx_msg_type= APT_MGMSG_PZ_SET_PICONSTS;
    node.tx_msg_data =QString::number(p).append(',').append(QString::number(integral));
    m_writeWorker.SendCmd(node);
}
QString Controller::GetInputSrcDisplay()
{
    INPUTSRC_MODE mode =m_model->GetInputSourceMode();
    QString strInput="";
    if(m_model->GetControlMode()==OPEN_LOOP)
    {
        switch (mode) {
        case INPUTSRC_SOFTWARE:
            strInput =tr("Software Only");
            break;
        case INPUTSRC_EXT:
        {
            if(m_model->GetHubAnalogInput()==INPUT_HUB_1)
                strInput=tr("Hub Analog In 1 + Software");
            else if(m_model->GetHubAnalogInput()==INPUT_HUB_2)
                strInput=tr("Hub Analog In 2 + Software");
            else
                strInput=tr("SMA Input + Software");
            break;
        }
        case INPUTSRC_PTM:
        {
            strInput=tr("Potentiometer + Software");
            break;
        }
        case INPUTSRC_PTM_EXT:
        {
            if(m_model->GetHubAnalogInput()==INPUT_HUB_1)
                strInput=tr("Hub In 1 +Pot. + Software");
            else if(m_model->GetHubAnalogInput()==INPUT_HUB_2)
                strInput=tr("Hub In 2 + Pot. + Software");
            else
                strInput=tr("SMA Input + Pot. + Software");

            break;
        }
        default:
            strInput=tr("Unknow Input");
            break;
        }
    }
    return strInput;
}
void Controller::receive_messages(DEVICE_MESSAGE msg)
{
    QMessageBox msgBox;
    if(msg<=100)//error
    {
        DisConnectDevice();
        msgBox.setIcon(QMessageBox::Critical);
    }
    switch (msg) {
    case READ_THREAD_ERROR:
        msgBox.setText("Read error!");
        break;
    case WRITE_THREAD_ERROR:
        msgBox.setText("Write error!");
        break;
    default:
        break;
    }
    msgBox.exec();

}
