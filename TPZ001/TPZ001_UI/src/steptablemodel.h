/************************************************************************/
// FILE NAME: addsinewavediag.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: The model of step list view.
/************************************************************************/
#ifndef STEPTABLEMODEL_H
#define STEPTABLEMODEL_H

#include <QAbstractTableModel>
#include<QList>
#include "PointStep.h"

class StepTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit StepTableModel(QList<PointStep*>* steps=NULL,QObject *parent = 0);
    void AddStep(PointStep* ptStep);
    void InsertStep(PointStep* ptStep,int curRow);
    void AddSteps(QList<PointStep*>& steps);
    void InsertSteps(QList<PointStep*>& steps, int curRow);
    void Remove(QList<int> & list);
    void Clear();
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role=Qt::EditRole);
      PointStep* takeStep(int logicalindex,int newindex);
signals:

public slots:

private:
    QList<PointStep*> *m_listSteps;
    int m_rowCnt;
};

#endif // STEPTABLEMODEL_H
