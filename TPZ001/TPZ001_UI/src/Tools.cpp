#include"Tools.h"
QJsonObject CreateObj(QString name, bool isquery, int returnType,QString unit,QList<Parameter> & para)
{
    QJsonObject obj;
    obj.insert("Name",name);
    obj.insert("IsQuery",isquery);
    obj.insert("ReturnType",returnType);
    obj.insert("Unit",unit);
    if(para.count()>0)
    {
        QJsonArray par;
        for(int i=0;i<para.count();i++)
        {
            QJsonObject parObj;
            parObj.insert("Name",para.at(i).name);
            parObj.insert("ParamType",para.at(i).type);
            parObj.insert("Max",para.at(i).max);
            parObj.insert("Min",para.at(i).min);
            parObj.insert("Unit",para.at(i).unit);
            par.append(parObj);
        }
        obj.insert("Parameters",par);
    }
    return obj;
}
