/************************************************************************/
// FILE NAME: addsinewavediag.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION:Used to sync data between device and GUI
/************************************************************************/
#ifndef SYNCDEVICEWORKER_H
#define SYNCDEVICEWORKER_H

#include <QObject>
#include "writeworker.h"

class SyncDeviceWorker : public QObject
{
    Q_OBJECT
public:
    explicit SyncDeviceWorker(QObject *parent = 0);
    void Init(int handle,WriteWorker * worker){
        m_handle = handle;
        m_worker = worker;
    }
public slots:
    void DoWork();
private:
    void ReqOutputVolt();
    void ReqControlMode();
    void ReqMaxVolt();
    void ReqInputSrc();
    void ReqPIConsts();
    void ReqDISPIntensity();
    int m_handle;
    int m_pastCycles;
    int m_SendAlive;
    WriteWorker *m_worker;
};

#endif // SYNCDEVICEWORKER_H
