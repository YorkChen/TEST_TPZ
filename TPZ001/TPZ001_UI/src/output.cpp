#include "output.h"
#include<QDebug>
output::output(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::output)
{
    ui->setupUi(this);
}

output::~output()
{
    delete ui;
}
void output::Init(Controller *ctrl,DeviceModel* model)
{
    m_controller = ctrl;
    m_model = model;
    InitConnections();
    BindDataSource();
}
void output::BindDataSource()
{
    m_binding.insert("OUTPUTVOLT",&output::UpdateVolt);
    m_binding.insert("CONTROLMODE",&output::UpdateControlMode);
    m_binding.insert("INPUTSOURCEMODE",&output::UpdateInputSourceMode);
    m_binding.insert("MAXVOLT",&output::UpdateMaxVolt);
    m_binding.insert("JOGSTEPSIZE",&output::UpdateJogStep);

}
void output::UpdateVolt()
{
    ui->voltLabel->display(m_model->GetOutputVolt());
}
void output::UpdateInputSourceMode()
{
    ui->lblInputSrc->setText(m_controller->GetInputSrcDisplay());
}
void output::UpdateMaxVolt()
{
    ui->lblMaxVolt->setText(QString::number(m_model->GetMaxVolt()));
}
void output::UpdateJogStep()
{

    double c = m_model->GetJogStepSize();
    ui->lblJogStepVolt->setText(QString::number(c));
}
void output::UpdateControlMode()
{
    if(m_model->GetControlMode()==OPEN_LOOP ||
            m_model->GetControlMode() == OPEN_LOOP_SMOOTH)
        ui->lblLoopMode->setText("Open Loop");
    else
        ui->lblLoopMode->setText("Closed Loop");
}

void output::InitConnections()
{
    connect(m_model,SIGNAL(PropertyChanged(QString)),
            this,SLOT(ModelPropertyChanged(QString)),Qt::QueuedConnection);
}
void output::ModelPropertyChanged(QString propName)
{
    QString name = propName.toUpper();
    if(m_binding.contains(name))
    {
        fooPtr  fp = m_binding.value(name);
        (this->*fp)();
    }
}
