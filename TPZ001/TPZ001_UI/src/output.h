/************************************************************************/
// FILE NAME: output.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: Display output voltage, loop type, input source,jog step,and so on.
/************************************************************************/
#ifndef OUTPUT_H
#define OUTPUT_H

#include <QWidget>
#include "ui_output.h"
#include"Controller.h"
#include"devicemodel.h"
namespace Ui {
class output;
}

class output : public QWidget
{
    Q_OBJECT

public:
    explicit output(QWidget *parent = 0);
    ~output();
    Ui::output *ui;
    void BindDataSource();
    void Init(Controller *ctrl,DeviceModel* model);

    void InitConnections();
private:
    void UpdateMaxVolt();
    void UpdateVolt();
    void UpdateJogStep();
    void UpdateControlMode();
    void UpdateInputSourceMode();
private:
    typedef void (output::*fooPtr)();
    Controller *m_controller;
    QHash<QString,fooPtr> m_binding;
    DeviceModel *m_model;
public slots:
    void ModelPropertyChanged(QString propName);
};

#endif // OUTPUT_H
