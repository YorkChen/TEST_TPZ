#include "InputWidget.h"
#include "ui_InputWidget.h"

#include<QDebug>
#include"dragabletableview.h"
#define OUPUT_TO_DEVICE_INTERVAL 100//ms
#define DEFAULT_STEP_INTERVAL 5 //s
InputWidget::InputWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InputWidget)
{
    ui->setupUi(this);
    m_stepTimer.setSingleShot(true);
//    m_stepTimer.setInterval(OUPUT_TO_DEVICE_INTERVAL);
    m_doubleDel =new DoubleDelegate(this);
    m_stepTableModel=new StepTableModel(&m_listSteps);

    m_CycleDialog=new SetCycle(this);
    m_CycleDialog->setWindowTitle(tr("Set cycles"));
    m_CycleDialog->hide();

    ui->tableView->setModel(m_stepTableModel);
    ui->tableView->setItemDelegateForColumn(1,m_doubleDel);
    ui->tableView->setItemDelegateForColumn(2,m_doubleDel);

    //set dragable
    ui->tableView->verticalHeader()->setSectionsMovable(true);
    ui->tableView->hideColumn(0);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    m_pauseIcon = QIcon("://Resources/1414162722_button-pause_basic_blue.png");
    m_playIcon = QIcon(":/Resources/1414162778_button-play_basic_blue.png");
    m_isRuning=false;
    m_cycles = 1;
    m_leftCycles  = 1;
    m_curModelIdx = 0;
    m_pastTime = 0;
    m_jogStepSize = 1;
    m_stepTimerInterval = OUPUT_TO_DEVICE_INTERVAL/(double)1000;
    ui->count_label->setVisible(false);
    connect(&m_stepTimer,SIGNAL(timeout()),this,SLOT(OnTimerTimeout()));

}

InputWidget::~InputWidget()
{
    delete ui;
}
void InputWidget::init(Controller *ctrl, DeviceModel *model)
{
    m_controller=ctrl;
    m_model=model;
    InitConnections();
    SetCycles(m_leftCycles);
}
void InputWidget::InsertStep(int curRow)
{
    PointStep* step =new PointStep();
    step->SetDuration(DEFAULT_STEP_INTERVAL);
    step->SetVolt(0);
    m_stepTableModel->InsertStep(step,curRow);
}


void InputWidget::RemoveSteps(QList<int>& list)
{
    m_stepTableModel->Remove(list);
}

void InputWidget::RunSteps()
{
    if(m_listSteps.count()<=0)
    {
        return;
    }
    if(!m_isRuning)
    {
        m_curModelIdx =-1;
        GoToNextStep();
        m_stepTimer.start();
        SetIsStepRunning(true);
    }
    UpdateIsStepRunning();
}

void InputWidget::PauseSteps()
{
//    m_stepTimer.stop();
    SetIsStepRunning(false);
    UpdateIsStepRunning();
}

void InputWidget::OnTimerTimeout()
{
//    m_pastTime += m_stepTimerInterval;
//    if(m_pastTime >= m_listSteps[m_curModelIdx]->GetDuration())
    {
        if(!GoToNextStep())
        {
            PauseSteps();
        }
    }
}

bool InputWidget::GoToNextStep()
{
    if(m_curModelIdx>=m_listSteps.count()-1)
    {
        if(m_leftCycles == 1)
        {
            SetCycles(m_CycleDialog->GetCycles());
            return false;
        }
        else if(m_leftCycles > 1)
        {
            SetCycles(m_leftCycles - 1);
        }
        //Coninuous

    }
    m_curModelIdx =(m_curModelIdx+1)%m_listSteps.count();
    SetVolt(m_listSteps[m_curModelIdx]->GetVolt());
    UpdateStepIndex();
    m_pastTime =0;
    m_stepTimer.start(m_listSteps.at(m_curModelIdx)->GetDuration()*1000);
    return true;
}
void InputWidget::SetVolt(double v)
{
    m_controller->SetVolt(v);
}
void InputWidget::on_btnAdd_clicked()
{
    PointStep* step =new PointStep();
    step->SetDuration(DEFAULT_STEP_INTERVAL);
    step->SetVolt(0);
    m_stepTableModel->InsertStep(step,ui->tableView->currentIndex().row());

}

void InputWidget::on_btnRemove_clicked()
{
    QModelIndexList indexes = ui->tableView->selectionModel()->selectedRows();
    QList<int> rows;
    for(int i=0;i<indexes.count();i++)
    {
        rows.append( indexes.at(i).row());
    }

    qSort(rows);
    m_stepTableModel->Remove(rows);
}

void InputWidget::on_btnPlay_clicked()
{
    if(m_stepTableModel->rowCount(QModelIndex())<=0)
    {
        return;
    }
    if(m_isRuning)
    {
        PauseSteps();
    }
    else
    {
        RunSteps();
    }
}
void InputWidget::on_extPlay_clicked()
{
    if(m_CycleDialog->exec())
    {
        int c=m_CycleDialog->GetCycles();
        m_cycles=c;
        SetCycles(c);
    }
    else
    {
        m_CycleDialog->SetCycles(m_cycles);
    }
}
void InputWidget::UpdateStepIndex()
{
    ui->tableView->selectRow(m_curModelIdx);
}

void InputWidget::UpdateIsStepRunning()
{
    ui->tableView->setEnabled(!m_isRuning);
    ui->btnAdd->setEnabled(!m_isRuning);
    ui->btnRemove->setEnabled(!m_isRuning);

    if(m_isRuning) {
        ui->count_label->setVisible(true);
        ui->extPlay->setVisible(false);
        ui->btnPlay->setIcon(m_pauseIcon);
    }
    else {
        if(m_model->GetDeviceIsConnect())
        {
            ui->count_label->setVisible(false);
            ui->extPlay->setVisible(true);
            ui->btnPlay->setIcon(m_playIcon);
        }
    }
}

void InputWidget::UpdateCycles()
{
    int c = m_leftCycles;
    QString d = (c>=0) ? (QString::number(c)):("Inf");
    ui->count_label->setText(d);
}
void InputWidget::InitConnections()
{
    connect(m_doubleDel,SIGNAL(EditorCreated(QModelIndex)),
            this,SLOT(EditorCreated(QModelIndex)));
}

void InputWidget::EditorCreated(const QModelIndex &modelIndex)
{
    if(modelIndex.column()==1)
    {
        m_doubleDel->SetRange(0,m_model->GetMaxVolt());
    }
    else if(modelIndex.column()==2)
    {
        m_doubleDel->SetRange(0,1000000);
    }
}

