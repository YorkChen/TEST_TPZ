/************************************************************************/
// FILE NAME: doubledelegate.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: Delegate of step list view
/************************************************************************/
#ifndef DOUBLEDELEGATE_H
#define DOUBLEDELEGATE_H

#include <QItemDelegate>
#include <QModelIndex>
#include <QObject>
#include <QWidget>
#include <QApplication>
#include <QString>
#include <QDoubleSpinBox>
#include <QList>

class DoubleDelegate : public QItemDelegate
{
    Q_OBJECT

public:
    DoubleDelegate(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;

    void setEditorData(QWidget *editor, const QModelIndex &index) const;

    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const;

    void updateEditorGeometry(QWidget *editor,
                              const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void SetRange(double min,double max);
    void SetTrimLength(int m){
        m_trimLength = m;
    }

signals:
    void valuechanged(double newValue,const QModelIndex &modelIndex);
    void EditorCreated(const QModelIndex & modelIndex);
private:
    void SetValue(double value){m_preValue = value;}
    double m_preValue;
    double m_min;
    double m_max;
    double m_trimLength;
};

#endif // DOUBLEDELEGATE_H
