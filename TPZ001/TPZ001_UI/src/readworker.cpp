#include "readworker.h"
#include"../../TPZ001_CMD/src/tpz001_cmd.h"

#include <QThread>
#include <QDateTime>
#include<QDebug>
#define MAX_READ_MSG_LENGTH 128
#define READ_INTERVAL 100 //ms

ReadWorker::ReadWorker(QObject *parent) :
    QThread(parent)
{
    m_handle = -1;
    m_stopped =false;
    InitFuncMap();
}

void ReadWorker::InitFuncMap()
{
    m_readFuncMap.insert(APT_MGMSG_PZ_GET_OUTPUTVOLTS,&ReadWorker::ReadOutPutVolt);
    m_readFuncMap.insert(APT_MGMSG_PZ_GET_POSCONTROLMODE,&ReadWorker::ReadControlMode);
    m_readFuncMap.insert(APT_MGMSG_PZ_GET_IOSETTINGS,&ReadWorker::ReadIOSettings);
    m_readFuncMap.insert(APT_MGMSG_PZ_GET_INPUTVOLTSSRC,&ReadWorker::ReadInputSrc);
    m_readFuncMap.insert(APT_MGMSG_PZ_GET_PICONSTS,&ReadWorker::ReadPIConsts);
    m_readFuncMap.insert(APT_MGMSG_PZ_GET_TPZ_DISPSETTINGS,&ReadWorker::ReadDISPSettings);
}

void ReadWorker::Init(int handle, DeviceModel * model)
{
    m_handle = handle;
    m_model = model;
    m_stopped = false;
    m_quit = false;
}

void ReadWorker::run()
{
    DoWork();
}

void ReadWorker::DoWork()
{

    int r=1;
    while(!m_stopped) {
        while(r>0){
            r = fnTPZ001_DLL_ReadDataType(m_handle,&msg);
            if(r ==1) {
                Proc();
            }
            else if(r<0) //error, device might be removed.
            {
                emit deviceDisconnected();
            }
        }
        msleep(READ_INTERVAL);
        r = 1;
    }
    m_quit = true;
}

void ReadWorker::Proc()
{
    char oBuf[MAX_READ_MSG_LENGTH];
    int r;
    r = fnTPZ001_DLL_ReadData(m_handle,oBuf,msg.res_msg_type,msg.res_msg_size);
    if(r > 0 && r < MAX_READ_MSG_LENGTH)
    {
        if(m_readFuncMap.contains(msg.res_msg_type))
        {
            ReadPtr fptr =  m_readFuncMap.value(msg.res_msg_type);
            (this->*fptr)(oBuf);
        }
    }
}

void ReadWorker::ReadOutPutVolt(char *oBuf)
{
    APT_MGMSG_PZ_GET_OUTPUTVOLTS_T *apt_data =
            (APT_MGMSG_PZ_GET_OUTPUTVOLTS_T *)oBuf;
    m_model->SetOutputVolt(apt_data->volts);
}

void ReadWorker::ReadControlMode(char *oBuf)
{
    APT_MGMSG_PZ_GET_CONTROLMODE_T *apt_data =
            (APT_MGMSG_PZ_GET_CONTROLMODE_T *)oBuf;
    m_model->SetControlMode((CONTROL_MODE)apt_data->mode);
}

void ReadWorker::ReadIOSettings(char *oBuf)
{
    APT_MGMSG_PZ_GET_IOSETTINGS_T *apt_data =
            (APT_MGMSG_PZ_GET_IOSETTINGS_T *)oBuf;
    m_model->SetMaxVolt(apt_data->maxVolt);
    m_model->SetHubAnalogInput((HUB_ANALOGINPUT)apt_data->hubAnalog);
}

void ReadWorker::ReadInputSrc(char *oBuf)
{
    APT_MGMSG_PZ_GET_INPUTVOLTSOURCE_T *apt_data =
            (APT_MGMSG_PZ_GET_INPUTVOLTSOURCE_T*)oBuf;

    m_model->SetInputSourceMode((INPUTSRC_MODE)apt_data->mode);
}

void ReadWorker::ReadPIConsts(char *oBuf)
{
    APT_MGMSG_PZ_GET_PICONSTS_T *apt_data =
            (APT_MGMSG_PZ_GET_PICONSTS_T*)oBuf;
    m_model->SetLoopProp(apt_data->prop);
    m_model->SetLoopIntegral(apt_data->integral);
}

void ReadWorker::ReadDISPSettings(char *oBuf)
{
    APT_MGMSG_PZ_GET_DISPSETTING_T *apt_data =
            (APT_MGMSG_PZ_GET_DISPSETTING_T*)oBuf;

    m_model->SetIntensity(apt_data->intensity);
}
