#include "steptablemodel.h"

StepTableModel::StepTableModel(QList<PointStep*>* steps,QObject *parent) :
    QAbstractTableModel(parent)
{
    m_rowCnt = 3;
    m_listSteps = steps;
}

int StepTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_listSteps->size();
}

int StepTableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_rowCnt;
}

QVariant StepTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= m_listSteps->size() || index.row() < 0)
        return QVariant();

    if (role == Qt::DisplayRole) {
        PointStep* model  = m_listSteps->at(index.row());

        if (index.column() == 0)
        {
            return model->GetId();
        }
        else if (index.column() == 1)
            return QString::number(model->GetVolt(),'f',2);
        else if(index.column() == 2)
            return QString::number(model->GetDuration());
    }
    return QVariant();
}

QVariant StepTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return tr("ID");
        case 1:
            return tr("Voltage (v)");
        case 2:
            return tr("Duration (s)");
        default:
            return QVariant();
        }
    }
    else if(orientation == Qt::Vertical)
    {
        return QString("  %1").arg(section);
    }
    return QVariant();
}

bool StepTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {
        int row = index.row();

        PointStep* ptStep = m_listSteps->at(row);
        if (index.column() == 1)
            ptStep->SetVolt(value.toDouble());
        else if (index.column() == 2)
            ptStep->SetDuration(value.toDouble());
        else
            return false;

        emit(dataChanged(index, index));

        return true;
    }

    return false;
}

Qt::ItemFlags StepTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    if(index.column()==0)
        return QAbstractTableModel::flags(index)& ~Qt::ItemIsEditable;

    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

void StepTableModel::InsertStep(PointStep* ptStep,int curRow)
{
    if(curRow<0) curRow = -1;
    int insertIdx =curRow + 1;
//    if(curRow<0) insertIdx=m_listSteps->count();
    beginInsertRows(QModelIndex(),insertIdx,insertIdx);
    for(int i=insertIdx;i<m_listSteps->count();i++)
        m_listSteps->at(i)->SetId(m_listSteps->at(i)->GetId()+1);
    ptStep->SetId(insertIdx+1);
    m_listSteps->insert(insertIdx,ptStep);

    endInsertRows();
    QModelIndex topIndex =createIndex(m_listSteps->count()-1,0/*,0*/);
    QModelIndex bottomIndex =createIndex(m_listSteps->count()-1,m_rowCnt);
    emit dataChanged(topIndex,bottomIndex);
}

void StepTableModel::AddStep(PointStep* ptStep)
{
    InsertStep(ptStep,-1);
}

void StepTableModel::AddSteps(QList<PointStep*>& steps)
{
    InsertSteps(steps,m_listSteps->count()-1);
}

void StepTableModel::InsertSteps(QList<PointStep*>& steps, int curRow)
{
    if(curRow<0) curRow = -1;
    int insertIdx = curRow + 1;
//    if(curRow<0) insertIdx=m_listSteps->count();
    beginInsertRows(QModelIndex(),insertIdx,insertIdx+steps.count()-1);
    for(int i=insertIdx;i<m_listSteps->count();i++)
        m_listSteps->at(i)->SetId(m_listSteps->at(i)->GetId()+steps.count());
    for(int j=0;j<steps.count();j++)
    {
        steps.at(j)->SetId(insertIdx+j+1);
        m_listSteps->insert(insertIdx+j,steps.at(j));
    }
    endInsertRows();
    QModelIndex topIndex =createIndex(insertIdx,0/*,0*/);
    QModelIndex bottomIndex =createIndex(insertIdx+steps.count(),m_rowCnt);
    emit dataChanged(topIndex,bottomIndex);
}

void StepTableModel::Clear()
{
    if(m_listSteps->count()<=0)return;
    beginRemoveRows(QModelIndex(),0,m_listSteps->length()-1);
    m_listSteps->clear();
    endRemoveRows();

    QModelIndex topIndex =createIndex(0,0/*,0*/);
    QModelIndex bottomIndex =createIndex(m_listSteps->length()-1,m_rowCnt);
    emit dataChanged(topIndex,bottomIndex);
}

void StepTableModel::Remove(QList<int> & list)
{
    if(list.count()<=0) return;

    for(int i= list.count()-1;i >= 0;i--)
    {
        beginRemoveRows(QModelIndex(),list.at(i),list.at(i));
        m_listSteps->removeAt(list.at(i));
        endRemoveRows();
    }

    for(int j=list.at(0);j<m_listSteps->count();j++)
    {
        PointStep* step =m_listSteps->at(j);
        step->SetId(j+1);
    }

    QModelIndex topIndex =createIndex(list.at(0),0/*,0*/);
    QModelIndex bottomIndex =createIndex(list.last(),m_rowCnt);
    emit dataChanged(topIndex,bottomIndex);


}
PointStep* StepTableModel::takeStep(int logicalindex,int newindex)
{
    PointStep* step= m_listSteps->at(logicalindex);
    return step;
}
