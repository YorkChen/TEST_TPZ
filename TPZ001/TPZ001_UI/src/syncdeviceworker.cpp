#include "syncdeviceworker.h"
#include "apt_type.h"
#include<QDebug>
#include"controller.h"
#include<Windows.h>
#define REQ_SETTING_INTERVAL 10
SyncDeviceWorker::SyncDeviceWorker(QObject *parent)
    :QObject(parent)
{
    m_pastCycles= 0;
    m_SendAlive = 1;
}

void SyncDeviceWorker::DoWork()
{

    ReqOutputVolt();
    ReqControlMode();
    ReqMaxVolt();

    //Do not Req following value too frequently
    if(m_pastCycles == 0)
    {
        ReqInputSrc();
        ReqPIConsts();
        ReqDISPIntensity();
    }

    if(++m_pastCycles>REQ_SETTING_INTERVAL)
        m_pastCycles = 0;


    if(m_SendAlive*SYNC_INTERVAL>=1000)
    {
    write_node_t node;
    node.tx_device= m_handle;
    node.tx_msg_type=APT_MGMSG_MOT_ACK_DCSTATUSUPDATE;
    m_worker->SendCmd(node);
    m_SendAlive=1;
    }
    else
    {
        m_SendAlive++;
    }

}

void SyncDeviceWorker::ReqOutputVolt()
{
    write_node_t node;
    node.tx_device = m_handle;
    node.tx_msg_type =APT_MGMSG_PZ_REQ_OUTPUTVOLTS;

    m_worker->SendCmd(node);

}

void SyncDeviceWorker::ReqControlMode()
{
    write_node_t node;
    node.tx_device = m_handle;
    node.tx_msg_type = APT_MGMSG_PZ_REQ_POSCONTROLMODE;

    m_worker->SendCmd(node);

}

void SyncDeviceWorker::ReqMaxVolt()
{
    write_node_t node;
    node.tx_device = m_handle;
    node.tx_msg_type = APT_MGMSG_PZ_REQ_IOSETTINGS;

    m_worker->SendCmd(node);

}

void SyncDeviceWorker::ReqInputSrc()
{
    write_node_t node;
    node.tx_device =m_handle;
    node.tx_msg_type =APT_MGMSG_PZ_REQ_INPUTVOLTSSRC;
    m_worker->SendCmd(node);
}

void SyncDeviceWorker::ReqPIConsts()
{
    write_node_t node;
    node.tx_device =m_handle;
    node.tx_msg_type =APT_MGMSG_PZ_REQ_PICONSTS;
    m_worker->SendCmd(node);
}

void SyncDeviceWorker::ReqDISPIntensity()
{
    write_node_t node;
    node.tx_device =m_handle;
    node.tx_msg_type =APT_MGMSG_PZ_REQ_TPZ_DISPSETTINGS;
    m_worker->SendCmd(node);
}
