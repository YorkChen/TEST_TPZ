#include "SetCycle.h"


SetCycle::SetCycle(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetCycle)
{
    ui->setupUi(this);
}

SetCycle::~SetCycle()
{
    delete ui;
}

void SetCycle::on_rdoContinuous_clicked()
{
    if(ui->rdoContinuous->isChecked())
    {
        ui->spinCycle->setEnabled(false);
    }
}

void SetCycle::on_rdoCycle_clicked()
{
    if(ui->rdoCycle->isChecked())
        ui->spinCycle->setEnabled(true);
}

void SetCycle::on_buttonBox_rejected()
{
    reject();
}

void SetCycle::on_buttonBox_accepted()
{
    accept();
}
