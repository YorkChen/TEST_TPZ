/************************************************************************/
// FILE NAME: writenode.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: Defines the struct of data which send to device.
/************************************************************************/
#ifndef WRITENODE_H
#define WRITENODE_H
#include "apt_type.h"
class write_node_t
{
public:
    write_node_t()
    {
        tx_device = -1;
        tx_msg_type = -1;
        tx_volt = 0;
    }

    ~write_node_t()
    {
        tx_device = NULL;
    }

    void clean()
    {
        tx_msg_data.clear();
    }

    write_node_t& operator=(const write_node_t &other)
    {
        if (this==&other) return *this;
        tx_msg_data = other.tx_msg_data;
        tx_msg_type = other.tx_msg_type;
        tx_device = other.tx_device;
        tx_volt =other.tx_volt;
        tx_shortData = other.tx_shortData;
        return *this;
    }

    int tx_device;
    int tx_msg_type;
    double tx_volt;
    short tx_shortData;
    QString tx_msg_data;
};

#endif // WRITENODE_H
