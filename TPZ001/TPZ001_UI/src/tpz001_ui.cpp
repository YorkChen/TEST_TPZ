#include "tpz001_ui.h"
#include "ochestrator.h"
#include "../../TPZ001_CMD/src/tpz001_cmd.h"
#include <QList>
#include<QString>
#include<QMessageBox>
#include"FullScreen.h"
#include<QJsonArray>
#include<QJsonObject>
#include"Tools.h"
#include"transfer.h"
using std::string;
using std::list;
static QList<Ochestrator*> DeviceList;
QApplication* a;
char* actionStr=NULL;
Transfer* transferObj=new Transfer();
TPZ001_UISHARED_EXPORT int Load_UartLibrary()
{
    fnTPZ001_DLL_InitDevice(DeviceModel::dllPath.toLatin1().data(),"uart_library_ftdi.dll");
    return 0;
}
TPZ001_UISHARED_EXPORT void SetDllPath(char* path)
{
    DeviceModel::dllPath=path;
}
TPZ001_UISHARED_EXPORT int InitDevice()
{
    return LoadDevice("");
}

//PluginUI
TPZ001_UISHARED_EXPORT int InitUI(int handle)
{
    FullScreen* FWindow=static_cast<FullScreen*>(GetFScreen(handle));
    QObject::connect(transferObj,SIGNAL(m_signal_resize(int,int,int)),FWindow,SLOT(Resize(int,int,int)));
    return 0;
}
TPZ001_UISHARED_EXPORT int StartMsgLoop()
{
    int arg=0;
    a = new QApplication(arg,NULL);
    return a->exec();

}
TPZ001_UISHARED_EXPORT void StopMsgLoop()
{
    a->exit(0);
}
TPZ001_UISHARED_EXPORT void RemoveDevice(int handle)
{
    ExitDevice(handle);
}
TPZ001_UISHARED_EXPORT void Resize(int w,int h,int type)
{
    emit transferObj->m_signal_resize(w,h,type);
}

TPZ001_UISHARED_EXPORT HWND GetMainUI(int handle)
{
    FullScreen* FWindow=static_cast<FullScreen*>(GetFScreen(handle));
    QWidget* wid;
    wid=FWindow->MainView();
    wid->setParent(NULL);
    wid->move(0,0);
    wid->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    wid->show();
    return (HWND)wid->winId();
}

TPZ001_UISHARED_EXPORT HWND GetTopUI(int handle)
{
    FullScreen* FWindow=static_cast<FullScreen*>(GetFScreen(handle));
    QWidget* wid;
    wid=FWindow->TopView();
    wid->setParent(NULL);
    wid->move(0,0);
    wid->show();
    return (HWND)wid->winId();
}

TPZ001_UISHARED_EXPORT HWND GetStatusUI(int handle)
{
    HWND _wid=NULL;
    return _wid;
}

//DeviceSDK
TPZ001_UISHARED_EXPORT char* GetActions()
{
    if(actionStr!=NULL)
    {
       return actionStr;
    }
     QJsonArray actionAry;
     actionAry.append(CreateObj("JogUp",false,0));
     actionAry.append(CreateObj("JogDown",false,0));
     actionAry.append(CreateObj("Reset",false,0));
     actionAry.append(CreateObj("GetVolt",true,2,"V"));
     Parameter para;
     para.name="Volt";
     para.type=2;
     para.unit="V";
     para.max=150;
     para.min=0;
     QList<Parameter> pList;
     pList.append(para);
     actionAry.append(CreateObj("SetVolt",false,0,QString(),pList));

     QJsonDocument jsondoc(actionAry);
     QByteArray tmpAry=jsondoc.toJson();
     int size=tmpAry.size();
     size++;
     actionStr=new char[size];
     memcpy_s(actionStr,size,tmpAry.data(),size);
     return actionStr;
}
TPZ001_UISHARED_EXPORT void InvokeQueryAction(char* actionName,double& returnValue,int handle)
{
    if(strcmp("GetVolt",actionName)==0)
    {
        returnValue= DeviceList.at(handle)->GetModel()->GetOutputVolt();
    }
}
TPZ001_UISHARED_EXPORT void InvokeQueryAction_2(char* actionName,char* returnValue,int handle)
{

}
TPZ001_UISHARED_EXPORT void InvokeNonQueryAction(char* actionName, double paramValue,int handle)
{
    if(strcmp("SetVolt",actionName) == 0)
    {
        DeviceList.at(handle)->GetController()->SetVolt(paramValue);
    }
}
TPZ001_UISHARED_EXPORT void InvokeNonQueryAction_2(char* actionName, char* multiParams,int handle)
{
    SendAction(handle,actionName);
}

//IPluginController
TPZ001_UISHARED_EXPORT bool Connect(char* _sn,int handle)
{
    return DeviceList.at(handle)->GetController()->ConnectDevice(_sn);
}
TPZ001_UISHARED_EXPORT void Disconnect(int handle)
{
    DeviceList.at(handle)->GetController()->DisConnectDevice();
}
TPZ001_UISHARED_EXPORT bool IsConnected(int handle)
{
   return  DeviceList.at(handle)->GetModel()->GetDeviceIsConnect();
}
TPZ001_UISHARED_EXPORT void SetLocalization(char* lanId,int handle)
{

}

TPZ001_UISHARED_EXPORT void SetIsPauseSendingCmd(bool isPause,int handle)
{

}
TPZ001_UISHARED_EXPORT int GetAvailableDevices(char* devices)
{
    char sns[1024]={0};
    int r = fnTPZ001_DLL_GetDeviceList(sns,1024);
    if(r<1) return -1;
    QString str(sns);
    QStringList snsList = str.split(',');
    QStringList list;
    for(int i=0;i<snsList.count();i+=2)
    {
        if(snsList.at(i+1).contains("TPZ001"))
        {
            list.append(snsList.at(i));
        }
    }
    QString sn;
    for(int i=0;i<list.count();i++)
    {
        sn+=list.at(i)+",";
    }
    sn.remove(sn.size()-1,1);
    QByteArray tmpchar=sn.toLatin1();
    memcpy_s(devices,tmpchar.size()+1,tmpchar.data(),tmpchar.size()+1);
    return 0;
}


//UI interface
TPZ001_UISHARED_EXPORT int LoadDevice(QString sn)
{
    if(fnTPZ001_DLL_InitDevice(NULL,"uart_library_ftdi.dll")<0)
    {
        return -1;
    }
    Ochestrator *a;
    a = new Ochestrator(sn, 115200,5);
    if(a !=NULL) {
        DeviceList.push_back(a);
        return DeviceList.size()-1;
    }
    else
    {
        return -1;
    }
}

TPZ001_UISHARED_EXPORT QWidget* GetFScreen(int hDevice)
{
    QWidget *a;
    if(hDevice <0) return 0;
    else {
        if(hDevice < DeviceList.size()) {
            a = (DeviceList[hDevice]->GetFullScrren());
            return a;
        }
        else return 0;
    }
}

TPZ001_UISHARED_EXPORT QWidget* GetQScreen(int hDevice)
{
    QWidget *a;
    if(hDevice <0) return 0;
    else {
        if(hDevice < DeviceList.size()) {
            a = (DeviceList[hDevice]->GetQuaterScreen());
            return a;
        }
        else return 0;
    }
}

TPZ001_UISHARED_EXPORT int ExitDevice(int hDevice)
{

    if(DeviceList.count()>hDevice)
    {
        DeviceList[hDevice]->Exit();
        delete DeviceList[hDevice];
        if(fnTPZ001_DLL_ReleaseDevice(NULL)==0)
        {
            return -1;
        }
        return 0;
    }
    else
    {
        return -1;
    }

}

TPZ001_UISHARED_EXPORT int SendAction(int hDevice,QString cmd)
{
    if(hDevice < 0 || cmd.isEmpty())
    {
        return -1;
    }
    DeviceList[hDevice]->SendAction(cmd);
    return 0;
}
