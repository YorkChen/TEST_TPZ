/************************************************************************/
// FILE NAME: SetCycle.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: Set step list view's cycle number.
/************************************************************************/
#ifndef SETCYCLE_H
#define SETCYCLE_H

#include <QDialog>
#include "ui_SetCycle.h"
#include<QDebug>
namespace Ui {
class SetCycle;
}

class SetCycle : public QDialog
{
    Q_OBJECT

public:
    explicit SetCycle(QWidget *parent = 0);
    ~SetCycle();
    int GetCycles(){
        if(ui->rdoCycle->isChecked())
            return ui->spinCycle->value();
        else
            return -1;//continuous
    }

    void  SetCycles(int c)
    {
        if(c>=0)
        {
            ui->rdoCycle->setChecked(true);
            ui->spinCycle->setValue(c);
            on_rdoCycle_clicked();
        }
        else
        {
            ui->rdoContinuous->setChecked(true);
            on_rdoContinuous_clicked();
        }
    }

private slots:
    void on_rdoContinuous_clicked();

    void on_rdoCycle_clicked();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::SetCycle *ui;
};

#endif // EXTENDSTEPS_H
