#include "devicedialog.h"
#include "ui_devicedialog.h"

DeviceDialog::DeviceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DeviceDialog)
{
    ui->setupUi(this);
}

DeviceDialog::~DeviceDialog()
{
    delete ui;
}
void DeviceDialog::RefreshList(const QList<QString>& list)
{
    ui->devList->clear();
    for(int i=0;i<list.count();i++)
    {
        ui->devList->addItem(list.at(i));
    }
}
QString DeviceDialog::GetSN()
{
    return ui->devList->currentText();
}

void DeviceDialog::on_OKBtn_clicked()
{
    this->done(1);
}

void DeviceDialog::on_CancelBtn_clicked()
{
    this->done(0);
}
