#include "syncworker.h"
SyncWorker::SyncWorker(QObject *parent) :
    QThread(parent),m_stop(true)
{
}
void SyncWorker::run()
{
    while(!m_stop)
    {
        m_syncWorker.DoWork();
        QThread::msleep(SYNC_INTERVAL);
    }
}
void SyncWorker::Init(int handle, WriteWorker *ww)
{
    m_stop=false;
    m_syncWorker.Init(handle,ww);
}
