/************************************************************************/
// FILE NAME: writeworker.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION:Write thread,write data to device.
/************************************************************************/
#ifndef WRITEWORKER_H
#define WRITEWORKER_H

#include <QObject>
#include<QThread>
#include<QQueue>
#include<QMutex>
#include<QWaitCondition>
#include "writenode.h"
#include"config.h"

class WriteWorker : public QThread
{
    Q_OBJECT
public:
    explicit WriteWorker(QObject *parent = 0);
    void Init(int handle){ m_handle = handle; m_stopped =false;}
    void SendCmd(const write_node_t & node);

signals:
    void emit_error(DEVICE_MESSAGE);
    void finished();
public slots:
    void DoWork();
    void Stop()
    {
        m_mutex.lock();
        m_stopped = true;
        m_waitQueueNotEMpty.wakeAll();
        m_mutex.unlock();

        quit();
        wait();
        emit finished();
    }
protected:
    void run();
private:
    int Proc(const write_node_t& node);
    int m_handle;
    QQueue<write_node_t> m_queue;
    QMutex m_mutex;
    QWaitCondition m_waitQueueNotEMpty;
    bool m_stopped;
};

#endif // WRITEWORKER_H
