/************************************************************************/
// FILE NAME: controller.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: Defines controller
/************************************************************************/
#ifndef CONTROLLER_H
#define CONTROLLER_H

#include<QString>
#include<QThread>
#include<QObject>
#include<QTimer>
#include "devicemodel.h"
#include "readworker.h"
#include "writeworker.h"
#include "syncdeviceworker.h"
#include "writenode.h"
#include "steptablemodel.h"
#include "config.h"
#include"syncworker.h"
#define SYNC_INTERVAL 500 //ms

class Controller : public QObject
{
    Q_OBJECT
public:
    Controller(QString sn, int nbaud, int timeout,DeviceModel* model);
    ~Controller();
    QString GetSN(){
        return m_sn;
    }

public slots:
    void SetVolt(double v);
    bool ConnectDevice();
    bool ConnectDevice(QString sn);
    void DisConnectDevice();
    void Identify();
    void SetVoltLimit(double v);
    void GetDeviceList(QList<QString> &list);
    void receive_messages(DEVICE_MESSAGE);
public:
    void SetControlMode(CONTROL_MODE mode);
    void SetIOSettings(double maxVolt,HUB_ANALOGINPUT hubInput);
    void SetInputSrcMode(INPUTSRC_MODE mode);
    void SetIntensity(int intensity);
    void SetLoopConsts(int p,int integral);
    void StartProcDeviceMsg();
    void StopProcDeviceMsg();
    QString GetInputSrcDisplay();
    DeviceModel* m_model;
    QList<PointStep*> m_listSteps;
    int m_speed;
    int m_timeout;
    QString m_sn;
    int m_handle;
    ReadWorker m_readWorker;
    WriteWorker m_writeWorker;
    SyncDeviceWorker m_syncWorker;
    SyncWorker m_syncThread;
    bool m_stoped;
};

#endif // DEVICEMODELL_H
