#include "optionsview.h"
#include "ui_optionsview.h"
#include "config.h"
#include<QDebug>
OptionsView::OptionsView(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionsView)
{
    ui->setupUi(this);
    ui->cbxLoop->addItem("Open Loop",QVariant::fromValue((int)OPEN_LOOP));

    ui->cbxVoltRange->addItem("75",QVariant::fromValue(75));
    ui->cbxVoltRange->addItem("100",QVariant::fromValue(100));
    ui->cbxVoltRange->addItem("150",QVariant::fromValue(150));

    ui->cbxDriveSrc->addItem("Software(SW) Only",QVariant::fromValue((int)INPUTSRC_SOFTWARE));
    ui->cbxDriveSrc->addItem("Analog Input(Vin) +SW",QVariant::fromValue((int)INPUTSRC_EXT));
    ui->cbxDriveSrc->addItem("Potentiometer(Pot.) + SW",QVariant::fromValue((int)INPUTSRC_PTM));
    ui->cbxDriveSrc->addItem("Vin + Pot + SW",QVariant::fromValue((int)INPUTSRC_PTM_EXT));

    ui->cbxAnaInSrc->addItem("Hub Channel 1",QVariant::fromValue((int)INPUT_HUB_1));
    ui->cbxAnaInSrc->addItem("Hub Channel 2",QVariant::fromValue((int)INPUT_HUB_2));
    ui->cbxAnaInSrc->addItem("SMA Input",QVariant::fromValue((int)INPUT_SMA));
}

OptionsView::~OptionsView()
{
    delete ui;
    m_ctrl =NULL;
}

void OptionsView::Init(Controller *ctrl,DeviceModel* model)
{
    m_ctrl =ctrl;
    m_model=model;
    LoadConfig();
}

void OptionsView::LoadConfig()
{
    Config cfg =GetConfig();
    ui->cbxLoop->setCurrentIndex(ui->cbxLoop->findData(cfg.loopmode));
    ui->cbxVoltRange->setCurrentIndex(ui->cbxVoltRange->findData(cfg.maxVolt));
    ui->cbxAnaInSrc->setCurrentIndex(ui->cbxAnaInSrc->findData(cfg.hubAnalogInput));
    ui->cbxDriveSrc->setCurrentIndex(ui->cbxDriveSrc->findData(cfg.inputSrcMode));
    ui->spinIntegral->setValue(cfg.loopIntegral);
    ui->spinProp->setValue(cfg.loopPropotional);
    ui->spinIntensity->setValue(cfg.displayIntensity);
    ui->spinJogStep->setValue(cfg.jogStep);
}

void OptionsView::on_btnOK_clicked()
{
    Config cfg;
    cfg.loopmode = (CONTROL_MODE)ui->cbxLoop->itemData(ui->cbxLoop->currentIndex()).toInt();
    cfg.maxVolt =ui->cbxVoltRange->itemData(ui->cbxVoltRange->currentIndex()).toDouble();
    cfg.hubAnalogInput = (HUB_ANALOGINPUT)ui->cbxAnaInSrc->itemData(ui->cbxAnaInSrc->currentIndex()).toInt();
    cfg.inputSrcMode = (INPUTSRC_MODE)ui->cbxDriveSrc->itemData(ui->cbxDriveSrc->currentIndex()).toInt();
    cfg.jogStep = ui->spinJogStep->value();
    cfg.displayIntensity = ui->spinIntensity->value();
    cfg.loopIntegral =ui->spinIntegral->value();
    cfg.loopPropotional =ui->spinProp->value();
    ApplyConfig(cfg);
    this->accept();
}
void OptionsView::ApplyConfig(const Config& cfg)
{

    if(qAbs((m_model->GetMaxVolt() -cfg.maxVolt))>0.1
            ||cfg.hubAnalogInput!=m_model->GetHubAnalogInput())
    {
        m_ctrl->SetIOSettings(cfg.maxVolt,cfg.hubAnalogInput);
    }
    if(cfg.loopmode !=m_model->GetControlMode())
    {
        m_ctrl->SetControlMode(cfg.loopmode);
        m_model->SetControlMode(cfg.loopmode);
    }
    if(cfg.inputSrcMode != m_model->GetInputSourceMode())
    {
        m_ctrl->SetInputSrcMode(cfg.inputSrcMode);
        m_model->SetInputSourceMode(cfg.inputSrcMode);
    }

    m_ctrl->SetIntensity(cfg.displayIntensity);
    m_model->SetIntensity(cfg.displayIntensity);

    m_ctrl->SetLoopConsts(cfg.loopPropotional,cfg.loopIntegral);
    m_model->SetJogStepSize(cfg.jogStep);
}
Config OptionsView::GetConfig()
{
    Config cfg;
    cfg.loopmode = m_model->GetControlMode();
    cfg.maxVolt =m_model->GetMaxVolt();
    cfg.hubAnalogInput = m_model->GetHubAnalogInput();
    cfg.inputSrcMode =m_model->GetInputSourceMode();
    cfg.jogStep = m_model->GetJogStepSize();
    cfg.displayIntensity = m_model->GetIntensity();
    cfg.loopIntegral =m_model->GetLoopIntegral();
    cfg.loopPropotional =m_model->GetLoopProp();
    return cfg;
}
void OptionsView::on_btnCancel_clicked()
{
    this->reject();
}
void OptionsView::on_cbxLoop_currentIndexChanged(const QString &arg1)
{
    if(arg1=="Open Loop")
    {
        ui->spinProp->setEnabled(false);
        ui->spinIntegral->setEnabled(false);
        ui->cbxAnaInSrc->setEnabled(false);
    }
    else if(arg1=="Close Loop")
    {
        ui->spinProp->setEnabled(true);
        ui->spinIntegral->setEnabled(true);
        ui->cbxAnaInSrc->setEnabled(true);
    }
}
