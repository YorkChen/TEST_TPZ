#include "writeworker.h"
#include "apt_type.h"
#include "../../TPZ001_CMD/src/tpz001_cmd.h"
#include<QDebug>
#include<QString>
#include<QStringList>

WriteWorker::WriteWorker(QObject *parent) :
    QThread(parent)
{
    m_handle = -1;
}

void WriteWorker::SendCmd(const write_node_t &node)
{
    if(!m_stopped)
    {
        m_mutex.lock();
        m_queue.enqueue(node);
        m_waitQueueNotEMpty.wakeAll();
        m_mutex.unlock();
    }
}

void WriteWorker::run()
{
    DoWork();
}

void WriteWorker::DoWork()
{
    while(!m_stopped)
    {
        while (!m_queue.isEmpty()) {
            m_mutex.lock();
            write_node_t node = m_queue.dequeue();
            m_mutex.unlock();
            int ret = Proc(node);
            if(ret<0)
            {
                emit emit_error(WRITE_THREAD_ERROR);
                break;
            }
        }
        m_mutex.lock();
        if(m_queue.isEmpty()&&!m_stopped)
            m_waitQueueNotEMpty.wait(&m_mutex);
        m_mutex.unlock();
    }
    m_queue.clear();
}


int WriteWorker::Proc(const write_node_t &node)
{
    bool ok;
    int ret=0;
    switch (node.tx_msg_type)
    {
    case APT_MGMSG_PZ_REQ_OUTPUTVOLTS:
        ret = fnTPZ001_DLL_Req_Volts(node.tx_device);
        break;
    case APT_MGMSG_PZ_REQ_POSCONTROLMODE:
        ret = fnTPZ001_DLL_Req_ControlMode(node.tx_device);
        break;
    case APT_MGMSG_PZ_SET_POSCONTROLMODE:
        ret = fnTPZ001_DLL_Set_ControlMode(node.tx_device,(CONTROL_MODE)node.tx_shortData);
        break;
    case APT_MGMSG_MOD_IDENTIFY:
        ret = fnTPZ001_DLL_Identify(node.tx_device);
        break;
    case APT_MGMSG_PZ_SET_OUTPUTVOLTS:
        ret = fnTPZ001_DLL_Set_Volts(node.tx_device,node.tx_volt);
        break;
    case APT_MGMSG_PZ_REQ_IOSETTINGS:
        ret = fnTPZ001_DLL_Req_IOSettings(node.tx_device);
        break;
    case APT_MGMSG_PZ_SET_IOSETTINGS:
        ret = fnTPZ001_DLL_Set_IOSettings(node.tx_device,node.tx_volt,(unsigned short)node.tx_shortData);
        break;
    case APT_MGMSG_PZ_REQ_INPUTVOLTSSRC:
        ret = fnTPZ001_DLL_Req_InputSource(node.tx_device);
        break;
    case APT_MGMSG_PZ_SET_INPUTVOLTSSRC:
        ret = fnTPZ001_DLL_Set_InputSource(node.tx_device,(INPUTSRC_MODE)node.tx_shortData);
        break;
    case APT_MGMSG_PZ_REQ_PICONSTS:
        ret = fnTPZ001_DLL_Req_PIConsts(node.tx_device);
        break;
    case APT_MGMSG_PZ_SET_PICONSTS:
    {
        QStringList parts = node.tx_msg_data.split(',');
        ret = fnTPZ001_DLL_Set_PIConsts(node.tx_device,parts.at(0).toInt(),parts.at(1).toInt());
        break;
    }
    case APT_MGMSG_PZ_REQ_TPZ_DISPSETTINGS:
        ret = fnTPZ001_DLL_Req_DISPIntensity(node.tx_device);
        break;
    case APT_MGMSG_PZ_SET_TPZ_DISPSETTINGS:
    {
        ret = fnTPZ001_DLL_Set_DISPIntensity(node.tx_device,node.tx_shortData);
        break;
    }
    case APT_MGMSG_MOT_MOVE_JOG:
    {
        int en;
        en = node.tx_msg_data.toInt(&ok);
    }
        break;
    case APT_MGMSG_MOT_ACK_DCSTATUSUPDATE:
    {
        ret=fnTDC001_DLL_Ack_DCStatusUpdate(node.tx_device);
    }
        break;
    default:
        break;
    }

    return ret;
}
