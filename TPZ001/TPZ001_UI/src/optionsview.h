/************************************************************************/
// FILE NAME: optionsview.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: contains option settings
/************************************************************************/
#ifndef OPTIONSVIEW_H
#define OPTIONSVIEW_H

#include "Controller.h"
#include<QDialog>
namespace Ui {
class OptionsView;
}

class OptionsView : public QDialog
{
    Q_OBJECT

public:
    explicit OptionsView(QWidget *parent = 0);
    ~OptionsView();
    void Init(Controller *ctrl,DeviceModel*model);
    void ApplyConfig(const Config& cfg);
    Config GetConfig();
signals:
    void closed();
private slots:

    void on_btnOK_clicked();

    void on_btnCancel_clicked();

    void on_cbxLoop_currentIndexChanged(const QString &arg1);

private:
    void LoadConfig();
    Ui::OptionsView *ui;
    Controller *m_ctrl;
    DeviceModel* m_model;
};

#endif // OPTIONSVIEW_H
