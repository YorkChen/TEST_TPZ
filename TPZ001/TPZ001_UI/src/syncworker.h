#ifndef SYNCWORKER_H
#define SYNCWORKER_H

#include <QThread>
#include"syncdeviceworker.h"
#include"writeworker.h"
#define SYNC_INTERVAL 500 //ms
class SyncWorker : public QThread
{
    Q_OBJECT
public:
    explicit SyncWorker(QObject *parent = 0);
    void Init(int handle,WriteWorker* ww);
    void Stop()
    {
        m_stop=true;

        quit();
        wait();
    }

protected:
    void run();
private:
    SyncDeviceWorker m_syncWorker;
    bool m_stop;
signals:

public slots:

};

#endif // SYNCWORKER_H
