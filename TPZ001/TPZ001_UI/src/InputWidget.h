/************************************************************************/
// FILE NAME: InputWidget.h
// DATE:      2014. 9. 15
// COMPANY:		THORLABS
// Author:   York Chen
// DESCRIPTION: Contains step listview and operate buttons
/************************************************************************/
#ifndef INPUTWIDGET_H
#define INPUTWIDGET_H

#include <QWidget>
#include<QTimer>
#include"steptablemodel.h"
#include"controller.h"
#include"ui_InputWidget.h"
#include"doubledelegate.h"
#include"devicemodel.h"
#include "SetCycle.h"
namespace Ui {
class InputWidget;
}

class InputWidget : public QWidget
{
    Q_OBJECT

public:
    explicit InputWidget(QWidget *parent = 0);
    ~InputWidget();
    void init(Controller* ctrl,DeviceModel* model);
    void InsertStep(int curRow);
    void RunSteps();
    void RemoveSteps(QList<int>& list);
    void PauseSteps();

    bool GoToNextStep();
    void SetIsStepRunning(bool b){
        m_isRuning = b;
        m_model->SetIsStepRunning(b);
    }
    void SetCycles(int c){
        m_leftCycles  =c;
        UpdateCycles();
    }
    void SetVolt(double v);
    Ui::InputWidget *ui;
    void UpdateStepIndex();
    void UpdateIsStepRunning();
    void UpdateCycles();
    void InitConnections();
    QList<PointStep*> m_listSteps;
    StepTableModel * m_stepTableModel;
private:
    typedef void (InputWidget::*fooPtr)();
    QTimer m_stepTimer;
    int m_speed;
    int m_timeout;
    int m_cycles;
    int m_leftCycles;
    int m_curModelIdx;
    double m_pastTime;
    double m_stepTimerInterval;
    bool m_isRuning;
    bool m_stoped;
    double m_jogStepSize;
    Controller *m_controller;
    DeviceModel* m_model;
    DoubleDelegate *m_doubleDel;
    QIcon m_playIcon;
    QIcon m_pauseIcon;
    QHash<QString,fooPtr> m_binding;
    SetCycle* m_CycleDialog;
signals:
    void m_signal_stepIsRunning(bool);
public slots:

    void EditorCreated(const QModelIndex &modelIndex);

    void on_btnAdd_clicked();

    void on_btnRemove_clicked();

    void on_btnPlay_clicked();

    void on_extPlay_clicked();

    void OnTimerTimeout();

};

#endif // INPUTWIDGET_H
