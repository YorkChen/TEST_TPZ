#ifndef TOOLS_H
#define TOOLS_H
#include<QJsonObject>
#include<QJsonArray>
#include<QList>
class Parameter{
public:
    QString name;
    int type;//0:void 1:int 2:double 3:string 4:enum
    double max;
    double min;
    QString unit;
};

QJsonObject CreateObj(QString name, bool isquery, int returnType,QString unit=QString(),QList<Parameter> & para=QList<Parameter>());


#endif // TOOLS_H
