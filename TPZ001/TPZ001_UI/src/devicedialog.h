#ifndef DEVICEDIALOG_H
#define DEVICEDIALOG_H

#include <QDialog>

namespace Ui {
class DeviceDialog;
}

class DeviceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DeviceDialog(QWidget *parent = 0);
    ~DeviceDialog();
    void RefreshList(const QList<QString>& list);
    QString GetSN();
private slots:
    void on_OKBtn_clicked();

    void on_CancelBtn_clicked();

private:
    Ui::DeviceDialog *ui;
};

#endif // DEVICEDIALOG_H
