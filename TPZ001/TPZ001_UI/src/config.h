/************************************************************************/
// FILE NAME: config.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION: Defines config struct
/************************************************************************/
#ifndef CONFIG_H
#define CONFIG_H
#include "apt_type.h"
typedef struct
{
    double jogStep;
    double maxVolt;
    int displayIntensity;
    int loopPropotional;
    int loopIntegral;
    CONTROL_MODE loopmode;
    HUB_ANALOGINPUT hubAnalogInput;
    INPUTSRC_MODE inputSrcMode;
}Config;


enum DEVICE_MESSAGE{

    //0~100 for error ( include 100)
    READ_THREAD_ERROR,
    WRITE_THREAD_ERROR,

    //100~infinity for information


};



#endif // CONFIG_H
