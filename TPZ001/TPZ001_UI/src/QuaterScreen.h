/************************************************************************/
// FILE NAME: QuaterScreen.h
// DATE:      2014. 5 . 22
// COMPANY:		THORLABS
// Author:   Yongxin Liang
// DESCRIPTION:Quater screen, contains output widget and jog buttons
/************************************************************************/
#ifndef QuaterScreen_H
#define QuaterScreen_H

#include <QWidget>
#include<QIcon>
#include "controller.h"
namespace Ui {
class QuaterScreen;
}

class QuaterScreen : public QWidget
{
    Q_OBJECT

public:
    explicit QuaterScreen(QWidget *parent = 0);
    ~QuaterScreen();
    void Init(Controller *ctrl,DeviceModel* model);
protected:
    void closeEvent(QCloseEvent *);
public:
public slots:
    void ModelPropertyChanged(QString propName);
private slots:
    void on_btnConnect_clicked();

    void on_btnJogLeft_clicked();

    void on_btnReset_clicked();

    void on_btnJogRight_clicked();

private:
    void BindDataSource();
    void InitConnections();
    void UpdateConnection();
    typedef void (QuaterScreen::*fooPtr)();
    QHash<QString,fooPtr> m_binding;
    Ui::QuaterScreen *ui;
    Controller *m_controller;
    DeviceModel *m_model;
    bool m_IsModelUpdating;
    bool m_IsModifyVolt;
    QIcon m_connectIcon;
    QIcon m_disconnectIcon;
    double m_maxVolt;
    double m_minVolt;
    double m_spinStep;
};

#endif // QuaterScreen_H
