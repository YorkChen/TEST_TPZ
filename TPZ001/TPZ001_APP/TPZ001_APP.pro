#-------------------------------------------------
#
# Project created by QtCreator 2014-05-04T14:56:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TPZ001_APP
TEMPLATE = app
DESTDIR = ../../bin

SOURCES += ./src/main.cpp\

HEADERS  += ./src/tpz001_ui.h

RC_FILE = ./Resources/TPZ.rc
OTHER_FILES += \
    ./Resources/TPZ.rc

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../bin/ -lTPZ001_UI
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../bin/ -lTPZ001_UId

INCLUDEPATH += $$PWD/../../bin
DEPENDPATH += $$PWD/../../bin
