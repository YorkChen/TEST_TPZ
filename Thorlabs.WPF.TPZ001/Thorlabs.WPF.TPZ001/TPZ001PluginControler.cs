﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thorlabs.PluginInterfaces;
namespace Thorlabs.WPF.TPZ001
{
    
   public class TPZ001PluginControler : PluginControler
    {
        private int _handle;
        public TPZ001PluginControler()
        {
            _handle = -1;
        }
        
        public override void Connect(string sn)
        {
             _handle = NativeWrapper.fnTPZ001_DLL_OpenDevice(sn,115200,5);
           
        }

        public override void Disconnect()
        {
            NativeWrapper.fnTPZ001_DLL_CloseDevice(_handle);
            //return 0;
        }

        public override bool IsConnected()
        {
            if (_handle < 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public override void SetLocalization(string lanId)
        {
           // NativeWrapper.SetLocalization(lanId, _handle);
        }

 
    }
    
}
