﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Thorlabs.PluginInterfaces;
namespace Thorlabs.WPF.TPZ001
{
    
    class TPZ001SDK : DeviceSDK
    {
        private int _handle;
        private string _actions = string.Empty;
        Model _model;
        public TPZ001SDK(ref Model m)
        {
            _model = m;
            ReadActions();
        }
        private void ReadActions()
        {
            try
            {
                var file1 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TPZ001_Actions.json");
                using (StreamReader sr = new StreamReader(File.OpenRead(file1)))
                {
                    _actions = sr.ReadToEnd();
                }
            }
            catch (Exception)
            {
                _actions = string.Empty;
            }
        }

        public override string GetActions()
        {
            if (string.IsNullOrEmpty(_actions))
                ReadActions();
            return _actions;
        }


        public override void InvokeQueryAction(string actionName, out double value)
        {
            if (actionName == "GetVolt")
            {
                value = _model.Volt();
            }
            else
            {
                value = 0;
            }

        }

        public override void InvokeQueryAction(string actionName, out string value)
        {
            value = " ";
        }

        public override void InvokeNonQueryAction(string actionName, double paramValue)
        {

            
             
        }

        public override void InvokeNonQueryAction(string actionName, string multiParams)// string param or no param
        {
            if (actionName == "JogUp")
            {

            }
            else if (actionName == "JogDown")
            {

            }
        }
         
    }
     
}
