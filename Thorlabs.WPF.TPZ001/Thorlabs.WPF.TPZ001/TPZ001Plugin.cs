﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thorlabs.PluginInterfaces;
namespace Thorlabs.WPF.TPZ001
{
    public class TPZ001Plugin : DevicePlugin
    {
        private TPZ001PluginControler _pluginControler;
        private TPZ001PluginUI _pluginUI;
        private TPZ001SDK _pluginSDK;
        private int _handle;
        private ReadWorker _read;
        private WriteWorker _write;
        Model _model;
        public TPZ001Plugin()
        {
            _model = new Model();
            string path = AppDomain.CurrentDomain.BaseDirectory;
            _handle = NativeWrapper.fnTPZ001_DLL_InitDevice(path);
            
            _pluginControler = new TPZ001PluginControler();
            _pluginUI = new TPZ001PluginUI();
            _pluginSDK = new TPZ001SDK(ref _model);

           
            _read = new ReadWorker(_handle,ref _model);
            _write = new WriteWorker(_handle,ref _model);
        }

        public override PluginControler GetControler()
        {
            return _pluginControler;
        }

        public override IPluginUI GetPluginUI()
        {
           // return null;
            return _pluginUI;
        }

        public override DeviceSDK GetDeviceSDK()
        {
            return _pluginSDK;
        }
        public override void Dispose()
        {
            _pluginControler.Disconnect();
        }
    }
}
