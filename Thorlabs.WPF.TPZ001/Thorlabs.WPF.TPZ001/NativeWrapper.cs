﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Thorlabs.WPF.TPZ001
{
    public class NativeWrapper
    {

         [DllImport("TPZ001_CMD.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int fnTPZ001_DLL_InitDevice(string path);

         [DllImport("TPZ001_CMD.dll", CallingConvention = CallingConvention.Cdecl)]
         public static extern int fnTPZ001_DLL_ReleaseDevice(string path);

        [DllImport("TPZ001_CMD.dll", CallingConvention = CallingConvention.Cdecl)]
         public static extern int fnTPZ001_DLL_OpenDevice(string sn, int nbaud, int timeout);

        [DllImport("TPZ001_CMD.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int fnTPZ001_DLL_CloseDevice(int hDevice);

        [DllImport("TPZ001_CMD.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int fnTPZ001_DLL_GetDeviceList(StringBuilder nPort, int var);

        [DllImport("TPZ001_CMD.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int fnTPZ001_DLL_ReadDataType(int hDevice,  ref RESPONSE_MSG msg);

        [DllImport("TPZ001_CMD.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int fnTPZ001_DLL_ReadData(int hDevice, StringBuilder oBuf, ushort type, int size);

        [DllImport("TPZ001_CMD.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int fnTPZ001_DLL_ReadData(int hDevice, ref APT_MGMSG_PZ_GET_OUTPUTVOLTS_T oBuf, APT_RESPONSE_MESSAGE_TYPE type, int size);



        [DllImport("TPZ001_CMD.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int fnTPZ001_DLL_StartCtrl(int hDevice);

         [DllImport("TPZ001_CMD.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int fnTPZ001_DLL_Req_Volts(int hDevice);

         [DllImport("TPZ001_CMD.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int fnTPZ001_DLL_Req_Pos(int hDevice);

         [DllImport("TPZ001_CMD.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int fnTPZ001_DLL_Set_Volts(int hDevice,double volt);

  

        
    }
}
