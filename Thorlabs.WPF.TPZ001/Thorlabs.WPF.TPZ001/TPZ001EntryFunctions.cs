﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thorlabs.PluginInterfaces;
namespace Thorlabs.WPF.TPZ001
{
    public class TPZ001EntryFunctions : PluginEntry
    {
        
        public TPZ001EntryFunctions()
        {
            string dll_path = AppDomain.CurrentDomain.BaseDirectory;
            NativeWrapper.fnTPZ001_DLL_InitDevice(dll_path);
        }

        public override int GetAvailableDevices(out string devices)
        {
            StringBuilder str = new StringBuilder(200);
            NativeWrapper.fnTPZ001_DLL_GetDeviceList(str,1024);
            devices = str.ToString();
            return 0;
        }
       public  override DevicePlugin CreatePlugin()
        {
            TPZ001Plugin tpzPlugin = new TPZ001Plugin();
            return tpzPlugin;
        }

    }
}
