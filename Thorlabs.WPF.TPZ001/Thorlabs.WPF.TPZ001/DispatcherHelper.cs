﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Thorlabs.WPF.TPZ001
{
    public class DispatcherHelper
    {
        private DispatcherHelper() { }
        private static Dispatcher _patcher;

        public static Dispatcher Dispatcher
        {
            get { return _patcher; }
        }

        public static void Initialize()
        {
            _patcher = Dispatcher.CurrentDispatcher;
        }
    }
}
