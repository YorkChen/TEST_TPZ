﻿//using GalaSoft.MvvmLight.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thorlabs.PluginInterfaces;
using System.Windows;
using System.AddIn.Contract;
using System.AddIn.Pipeline;
using System.Threading;
namespace Thorlabs.WPF.TPZ001
{
    public class TPZ001PluginUI : IPluginUI
    {

        private FrameworkElement _toolBar;
        private FrameworkElement _frontPanel;
        private FrameworkElement _statusBar;
        static private Thread _mainThread=null;
        static int _deviceNum=0;
        private Object _lock = new Object();
        AutoResetEvent _autoR= new AutoResetEvent(false);
        private int _handle;
        public TPZ001PluginUI()
        {
            /*
            _handle = handle;
            _deviceNum++;
            if (_mainThread == null)
            {
                _mainThread = new Thread(MainLoop);
                _mainThread.SetApartmentState(ApartmentState.STA);
                _mainThread.Start();
            }
            if (DispatcherHelper.Dispatcher == null)
            {
                _autoR.WaitOne();
            }


            DispatcherHelper.Dispatcher.Invoke(() =>
            {

                NativeWrapper.InitUI(_handle);

            });
            
             */

        }
        public FrameworkElement GetTopView()
        {
            return null;
            /*
            if (_toolBar != null)
            {
                return _toolBar;
            }
            else
            {
                lock (_lock)
                {
                    _toolBar = new QtHwnd(65, 358, 1, _handle);
                    return _toolBar;
                }
            }
             * */
        }
        public FrameworkElement GetMainView()
        {
            return null;
            /*
            if (_frontPanel != null)
            {
                return _frontPanel;
            }
            else
            {
                lock (_lock)
                {
                    _frontPanel = new QtHwnd(300, 300, 0, _handle);
                    return _frontPanel;
                }
            }
             * */
        }
        public FrameworkElement GetStatusView()
        {
            return null;
            /*
            if (_statusBar != null)
            {
                return _statusBar;
            }
            else
            {
                lock (_lock)
                {
                    _statusBar = new QtHwnd(300, 300, 2, _handle);
                    return _statusBar;
                }
            }
             * */
        }
        private void MainLoop()
        {
            try
            {
                DispatcherHelper.Initialize();
                _autoR.Set();
            //    NativeWrapper.StartMsgLoop();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

    }
}
