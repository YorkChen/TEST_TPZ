#include"tpz001_cmd.h"
#include"Windows.h"
#include<iostream>
#define MAX_READ_MSG_LENGTH 100
static RESPONSE_MSG msg;
static int handle=0;

int ReadData(void)
{
    char oBuf[MAX_READ_MSG_LENGTH]={0};
    int r;
    r = fnTPZ001_DLL_ReadData(handle,oBuf,msg.res_msg_type,msg.res_msg_size);
    if(r > 0 && r < MAX_READ_MSG_LENGTH)
    {
        if(msg.res_msg_type==APT_MGMSG_PZ_GET_OUTPUTVOLTS)
        {
            APT_MGMSG_PZ_GET_OUTPUTVOLTS_T *apt_data =(APT_MGMSG_PZ_GET_OUTPUTVOLTS_T *)oBuf;
            std::cout<<"voltage : "<<apt_data->volts<<std::endl;
        }
    }
    return 0;
}

int main()
{
    // load uart_library
    if(fnTPZ001_DLL_InitDevice(NULL)<0)
    {
        std::cout<<"init failed"<<std::endl;
        return -1;
    }
    //connect to device
    handle=fnTPZ001_DLL_OpenDevice("81815512",115200,5);
    std::cout<<"handle :"<<handle<<std::endl;
    if(handle<0)
    {
        std::cout<<"connect error"<<std::endl;
        return -2;
    }

    int r=1;
    //Get MaxV. The max voltage must be get before reading or setting output voltage.
    fnTPZ001_DLL_Req_IOSettings(handle);
    Sleep(200);

    r= fnTPZ001_DLL_ReadDataType(handle,&msg);
    if(r==1)
    {
        ReadData();
    }


    //Set voltage

    double volt=35;
    int ret =fnTPZ001_DLL_Set_Volts(handle,volt);

    int i=0;
    //Read output voltage
    while(i<10) {
        fnTPZ001_DLL_Req_Volts(handle);

        Sleep(300);
        r = fnTPZ001_DLL_ReadDataType(handle,&msg);
        if(r ==1)
        {
            ReadData();
        }
        else if(r<0)
        {
            fnTPZ001_DLL_ReadData(handle,0,0,-1);
        }
        Sleep(100);
        i++;
    }

    fnTPZ001_DLL_CloseDevice(handle);
    return 0;
}
