#ifndef TDC001_CMD_H
#define TDC001_CMD_H
#include "apt_type.h"

#if defined(TDC001_CMD_LIBRARY)
#  define TDC001_CMDSHARED_EXPORT __declspec(dllexport)
#else
#  define TDC001_CMDSHARED_EXPORT __declspec(dllimport)
#endif

TDC001_CMDSHARED_EXPORT int InitDevice(char *path);
TDC001_CMDSHARED_EXPORT int OpenDevice(char* sn, int nbaud, int timeout);
TDC001_CMDSHARED_EXPORT int CloseDevice(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_StartCtrl(int hDevice);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_EableChannel(int hDevice, int en);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_ReqChanEnableState(int hDevice);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_MoveJog(int hDevice, unsigned char direction);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_MoveHome(int hDevice);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_MoveStop(int hDevice, unsigned char stop_mode);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_MoveAbsolute(int hDevice);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_MoveAbsolute(int hDevice, double distance);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_MoveVelocity(int hDevice, unsigned char direction);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_StatusUpdate(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_ReadDataType(int hDevice, RESPONSE_MSG *msg);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_ReadData(int hDevice, char *oBuf, unsigned short type, int size);


TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_VelParams(int hDevice, double min_vel, double acceleration, double max_vel);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_VelParams(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_HomeParams(int hDevice, unsigned short home_dir, unsigned short limit_switch,
                                        double home_vel, double offset_dis);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_HomeParams(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_JogParams(int hDevice, unsigned short jog_mode, double jog_step_size, double jog_min_vel,
                                       double jog_acc, double jog_max_vel, unsigned short jog_stop_mode);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_JogParams(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_GenMoveParams(int hDevice, double backlashdistance);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_GenMoveParams(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_LimitSwitchParams(int hDevice, unsigned short cw_hardlimit, unsigned short ccw_hardlimit,
                                               double cw_softlimit, double ccw_softlimit, unsigned short limit_mode);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_LimitSwitchParams(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_PotParamas(int hDevice, unsigned short zero_wnd, double vel1, unsigned short wnd1, double vel2,
                                        unsigned short wnd2, double vel3, unsigned short wnd3, double vel4);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_PotParamas(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_AVmodes(int hDevice, unsigned short modebites);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_AVmodes(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_ButtonParams(int hDevice, unsigned short mode, double position1,
                                          double position2, unsigned short timeout, unsigned short notused);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_ButtonParams(int hDevice);

/// restfactor/movefactor range: 1~100
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_PowerParams(int hDevice, unsigned short restfactor, unsigned short movefactor);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_PowerParams(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_Poscounter(int hDevice, double position);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_Poscounter(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_Enccounter(int hDevice, double encoder_counter);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_Enccounter(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_MoveRELParams(int hDevice, double relative_distance);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_MoveRELParams(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_MoveABSParams(int hDevice, double absolute_distance);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_MoveABSParams(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Set_DCPIDParams(int hDevice, long  proportional, long integral, long differential,
                                         long integral_limit, unsigned short filtercontrol);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_DCPIDParams(int hDevice);


TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_StartUpdateMsgs(int hDevice, unsigned char rate);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_StopUpdateMsgs(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Ack_DCStatusUpdate(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Req_StatusBits(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Sespend_EndofMoveMSGS(int hDevice);
TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Resume_EndofMoveMSGS(int hDevice);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_GetDeviceList(char *nPort, int var);

TDC001_CMDSHARED_EXPORT int fnTDC001_DLL_Identify(int hDevice);

#endif // TDC001_CMD_H
